import java.util.Scanner;
public class Task032 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите размер массивов: ");
        int k = sc.nextInt();
        int m[] = new int[k];
        int n[] = new int[k];
        int sum[] = new int[k];
        System.out.println("Заполните массив m:");
        for (int i = 0; i < k; i++){
            m[i] = sc.nextInt();
        }
        System.out.println("Заполните массив n:");
        for (int i = 0; i < k; i++){
            n[i] = sc.nextInt();
        }
        for (int i = 0; i < k; i++){
            sum[i] = m[i] + n[i];
            System.out.println(sum[i]);
        }
    }
}
