/**
 * Created by Alisher Zharbossynov from 11-602 on 01.11.16.
 */

// a-h соответсвуют 1-8 соответсвенно
import java.util.Scanner;

public class Chess {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите координаты для определения цвета клетки: ");
        Chess chessboard1 = new Chess();
        System.out.println(chessboard1.getColor(sc.nextInt(), sc.nextInt()));
        System.out.println("Введите 2 пары координат для пешки: ");
        System.out.println(chessboard1.getPawn(sc.nextInt(), sc.nextInt(), sc.nextInt(), sc.nextInt()));
        System.out.println("Введите 2 пары координат для коня:");
        System.out.println(chessboard1.getKnight(sc.nextInt(), sc.nextInt(), sc.nextInt(), sc.nextInt()));
        System.out.println("Введите 2 пары координат для ладьи: ");
        System.out.println(chessboard1.getCastle(sc.nextInt(), sc.nextInt(), sc.nextInt(), sc.nextInt()));
        System.out.println("Ведите 2 пары координат определения соседней фигуры: ");
        System.out.println(chessboard1.getNeighbour(sc.nextInt(), sc.nextInt(), sc.nextInt(), sc.nextInt()));

    }

    public String getColor(int i, int j){
        String message;
        if ((i + j) % 2 == 0){
            message = "Black";
            return message;
        }else{
            message = "White";
            return message;
        }
    }

    public String getPawn(int i, int j, int k, int m){  // Пешка может ходить только на 1 или 2 позиции вперед.
        String message;
        if ((i == k) && ((m - j) <= 2)){
            return message = "Yes";
        }else{
            return message = "No";
        }
    }

    public String getKnight(int i, int j, int k, int m){
        String message;
        if (((i == k-1 || i == k+1) && (j == m-2 || j == k+2)) || ((i == k-2 || i == k+2) && (j == m-1 || j == k+1))){
            return message = "Yes";
        }else{
            return message = "No";
        }
    }

    public String getCastle(int i, int j, int k, int m){
        String message;
        if (i == k || j == m){
            return message = "Yes";
        }else{
            return message = "No";
        }
    }

    public String getNeighbour(int i, int j, int k, int m){
        String message;
        if (i == k || j == m){
            return message = "Neghbour";
        }else{
            return message = "Same";
        }
    }


}
