package cw_23_03_17;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by MacBookAir on 23.03.17.
 *
 * Проверочная++
 * Задание номер 1.
 */

public class Task2 {

    public static List<String> matcher(List<String> list, Predicate<String> predicate){
        return list.stream().filter(predicate).collect(Collectors.toList());
    }
}
