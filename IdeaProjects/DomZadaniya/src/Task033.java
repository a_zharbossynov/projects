import java.util.Scanner;
public class Task033 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int j = 0;
        double countA = 0, countB = 0;
        System.out.println("Введите размер n для массивов a и b: ");
        int n = sc.nextInt();
        int a[] = new int[n];
        int b[] = new int[n];
        System.out.println("Заполните массив a: ");
        for (int i = 0; i < n; i++){
            a[i] = sc.nextInt();
        }
        System.out.println("Заполните массив b: ");
        for (int i = 0; i < n; i++){
            b[i] = sc.nextInt();
        }
        for (int i = 0; i < n; i++){
            int skalar = a[i]*b[i];
            j = skalar + j;
        }
        for (int i = 0; i < n; i++) {
            double modulA = (a[i]*a[i]);
            double modulB = (b[i]*b[i]);
            countA = modulA + countA;
            countB = modulB + countB;
        }
        double ugol = j/(Math.sqrt(countA) * Math.sqrt(countB));
        System.out.println("Скалярное произведение = " + j);
        System.out.println("Косинус угла между ними = " + ugol);
    }
}
