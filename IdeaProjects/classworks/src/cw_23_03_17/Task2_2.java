package cw_23_03_17;

import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by MacBookAir on 23.03.17.
 *
 * Проверочна++
 * Задание номер 2.
 */
public class Task2_2 {
    public static <T> List<T> matcher(List<T> list, Predicate<T> predicate){
        return list.stream().filter(predicate).collect(Collectors.toList());
    }
}
