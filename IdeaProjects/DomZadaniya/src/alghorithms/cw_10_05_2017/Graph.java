package alghorithms.cw_10_05_2017;

import java.util.ArrayList;
import java.util.LinkedList;

/**
 * Created by MacBookAir on 10.05.17.
 */

public class Graph {
    private ArrayList<Vertex> vertices;

    public Graph(int numberVertices){
        vertices = new ArrayList<Vertex>(numberVertices);
        for(int i = 0; i < numberVertices; i++){
            vertices.add(new Vertex("v" + Integer.toString(i)));
        }
    }


    public void addEdge(int src, int end, int weight){
        Vertex s = vertices.get(src);
        Edge new_edge = new Edge(vertices.get(end), weight);
        s.neighbors.add(new_edge);
    }


    public ArrayList<Vertex> getVertices() {
        return vertices;
    }


    public Vertex getVertex(int vert){
        return vertices.get(vert);
    }
}

class Edge{

    public final Vertex target;
    public final double weight;


    public Edge(Vertex target, double weight){
        this.target = target;
        this.weight = weight;
    }

}



class Vertex implements Comparable<Vertex>{

    public final String name;
    public ArrayList<Edge> neighbors;   //  список соседних вершин
    public LinkedList<Vertex> path;     // список пути
    public double minDistance = 1000 * 1000 * 1000;



    public int compareTo(Vertex anotherVert){
        return Double.compare(minDistance, anotherVert.minDistance);
    }


    public Vertex(String name){
        this.name = name;
        neighbors = new ArrayList<Edge>();
        path = new LinkedList<Vertex>();
    }

    public String toString(){
        return name;
    }
}