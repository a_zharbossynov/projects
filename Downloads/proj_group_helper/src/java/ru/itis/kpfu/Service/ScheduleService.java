package ru.itis.kpfu.Service;

import ru.itis.kpfu.Model.Lab;
import ru.itis.kpfu.Model.Lesson;
import ru.itis.kpfu.Model.Schedule;

import javax.xml.crypto.Data;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ScheduleService {

    public Schedule getSchedule(int group_id, int user_id){
        String raw = "SELECT * FROM timetables WHERE id_group = " + String.valueOf(group_id);
        Schedule schedule = new Schedule();

        try {
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);
            ResultSet set = statement.executeQuery();

            while(set.next()){
                Lesson lesson = new Lesson(set);
                schedule.addLesson(lesson);
            }

            connection.close();
            statement.close();
            set.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        ArrayList<Lesson> lessons = getLabLessons(user_id);
        for(Lesson lesson: lessons){
            schedule.addLesson(lesson);
        }

        return schedule;
    }
    public ArrayList<Lesson> getLabLessons(int user_id){
        ArrayList<Lesson> lessons = new ArrayList<>();
        String raw = "SELECT title, weekday, ordinal, auditorium FROM lab_timetables " +
                "INNER JOIN labs ON lab_timetables.id_lab = labs.id " +
                "WHERE id_lab = ANY (" +
                "SELECT lab_assignments.id_lab FROM lab_assignments " +
                "WHERE id_student = ?" +
                ") " +
                "ORDER BY weekday, ordinal";

        try{
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);
            statement.setInt(1, user_id);
            ResultSet set = statement.executeQuery();

            while(set.next()){
                Lesson lesson = new Lesson(set);
                lessons.add(lesson);
            }

            set.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return lessons;
    }
    public ArrayList<Integer> getAssignments(int user_id){
        String raw = "SELECT id_lab FROM lab_assignments WHERE id_student = ?";
        ArrayList<Integer> list = new ArrayList<>();

        try{
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);
            statement.setInt(1, user_id);
            ResultSet set = statement.executeQuery();

            while(set.next()){
                list.add(set.getInt("id_lab"));
            }

            set.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }
    public ArrayList<Lab> getLabs(){
        String raw = "SELECT * FROM labs";
        ArrayList<Lab> list = new ArrayList<>();

        try{
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);
            ResultSet set = statement.executeQuery();

            while(set.next()){
                Lab lab = new Lab(set);
                list.add(lab);
            }

            set.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return list;
    }

    public boolean addLesson(Lesson lesson, int id_group, int weekday){
        boolean inserted = false;
        String raw = "INSERT INTO timetables (id_group, weekday, ordinal, title, auditorium) " +
                "VALUES (?, ?, ?, ?, ?) " +
                "ON CONFLICT (id_group, weekday, ordinal) " +
                "DO UPDATE SET " +
                "(id_group, weekday, ordinal, title, auditorium) = (EXCLUDED.id_group, EXCLUDED.weekday, EXCLUDED.ordinal, EXCLUDED.title, EXCLUDED.auditorium) " +
                "RETURNING title";

        try {
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);

            statement.setInt(1, id_group);
            statement.setInt(2, weekday);
            statement.setInt(3, lesson.getOrdinal());
            statement.setString(4, lesson.getName());
            statement.setString(5, lesson.getAuditorium());

            ResultSet set = statement.executeQuery();

            if(set.next()){
                inserted = set.getString("title") != null;
            }

            connection.close();
            statement.close();
            set.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return inserted;
    }
    public boolean setHometask(String task, int id_group, int weekday, int ordinal){
        boolean updated = false;
        String raw = "UPDATE timetables " +
                "SET hometask = ? " +
                "WHERE id_group = ? AND weekday = ? AND ordinal = ? " +
                "RETURNING title";

        try{
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);

            statement.setString(1, task);
            statement.setInt(2, id_group);
            statement.setInt(3, weekday);
            statement.setInt(4, ordinal);

            ResultSet set = statement.executeQuery();
            if(set.next()) {
                updated = set.getString("title") != null;
            }

            connection.close();
            statement.close();
            set.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return updated;
    }

    public void clearTimetable(int id_group){
        String raw = "DELETE FROM timetables WHERE id_group = ?";

        try{
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);
            statement.setInt(1, id_group);
            ResultSet set = statement.executeQuery();

            connection.close();
            statement.close();
            set.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
    public void removeLesson(int id_group, int weekday, int ordinal){
        String raw = "DELETE FROM timetables " +
                "WHERE id_group = ? AND weekday = ? AND ordinal = ?";

        try{
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);

            statement.setInt(1, id_group);
            statement.setInt(2, weekday);
            statement.setInt(3, ordinal);

            ResultSet set = statement.executeQuery();

            connection.close();
            statement.close();
            set.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
