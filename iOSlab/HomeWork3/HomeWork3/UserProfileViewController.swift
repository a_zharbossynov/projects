//
//  UserProfileViewController.swift
//  HomeWork3
//
//  Created by MacBookAir on 24.09.17.
//  Copyright © 2017 iOSlab. All rights reserved.
//

import UIKit

class UserProfileViewController: UIViewController {

    var userNames = ["Jonathan Ive", "Hideo Kojima", "Craig Federighi", "Sergey Kuvaev",
                     "Dmitriy Shamov", "Lelouch Lamperouge", "Light Yagami"]
    
    var locationNames = ["Tokyo", "Kyoto", "Seoul", "Hong Kong", "Busan", "Toronto",
                         "Montreal", "Vancouver"]
    
    var userStatus = ["online (mob.)", "offline"]
    
    var randomIndex : Int = 0
    
    var userNameTitle = [""]
    
    @IBOutlet weak var userNameSurname: UILabel!
    
    @IBOutlet weak var webStatus: UILabel!
    
    @IBOutlet weak var location: UILabel!
    
    @IBOutlet weak var userAge: UILabel!
    
    @IBOutlet weak var userName: UINavigationItem!
    
    
    
    func generateRandomProfile() {
        userAge.text = String(arc4random_uniform(12) + 16)
        
        randomIndex = Int(arc4random_uniform(UInt32(userNames.count)))
        userNameSurname.text = userNames[randomIndex]
        
        userNameTitle = (userNameSurname.text?.components(separatedBy: " "))!
        userName.title = userNameTitle[0]
        
        randomIndex = Int(arc4random_uniform(UInt32(locationNames.count)))
        location.text = locationNames[randomIndex]
        
        randomIndex = Int(arc4random_uniform(UInt32(userStatus.count)))
        webStatus.text = userStatus[randomIndex]
        }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        generateRandomProfile()
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
