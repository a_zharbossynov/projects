public class Task051 {
    public static void main(String[] args) {
    }
}
class ComplexNumber {
    double a;
    double b;
    ComplexNumber(){
        a = 0;
        b = 0;
    }
    ComplexNumber(double a, double b){
        this.a = a;
        this.b = b;
    }
    public double getA(){
        return a;
    }
    public double getB() {
        return b;
    }

    ComplexNumber add(ComplexNumber CN){
        double a1 = a + CN.getA();
        double b1 = b + CN.getB();
        return new ComplexNumber(a1,b1);
    }
    void add2(ComplexNumber CN){
        a = a + CN.getA();
        b = b + CN.getB();
    }
    ComplexNumber sub(ComplexNumber CN){
        double a1 = a - CN.getA();
        double b1 = b - CN.getB();
        return new ComplexNumber(a1,b1);
    }
    void sub2(ComplexNumber CN){
        a = a - CN.getA();
        b = b - CN.getB();
    }
    ComplexNumber multNumber(double k){
        double a1 = a * k;
        double b1 = b * k;
        return new ComplexNumber(a1,b1);
    }
    void multNumber2(double m){
        a = a * m;
        b = b * m;
    }
    ComplexNumber mult(ComplexNumber CN){
        double a1 = a * CN.getA() + b * CN.getB() * (-1);
        double b1 = a * CN.getB() + b * CN.getA();
        return new ComplexNumber(a1,b1);
    }
    void mult2(ComplexNumber CN){
        a = a * CN.getA() + b * CN.getB() * (-1);
        b = a * CN.getB() + b * CN.getA();
    }
    ComplexNumber div(ComplexNumber CN){
        double a1 = (a*CN.getA() - b*CN.getB()*(-1)) / (CN.getA()*CN.getA() - CN.getB()*CN.getB()*(-1));
        double b1 = (CN.getA()*b - a*CN.getB()) / (CN.getA()*CN.getA() - CN.getB()*CN.getB()*(-1));
        return new ComplexNumber(a1,b1);
    }
    void div2(ComplexNumber CN){
        a = (a*CN.getA() - b*CN.getB()*(-1)) / (CN.getA()*CN.getA() - CN.getB()*CN.getB()*(-1));
        b = (CN.getA()*b - a*CN.getB()) / (CN.getA()*CN.getA() - CN.getB()*CN.getB()*(-1));
    }
    double length(){
        double m = Math.sqrt(a*a + b*b);
        return m;
    }
    public String toString(){
        String s = a + b + "i";
        return s;
    }
    double arg(){
        double argZ = Math.atan(b/a);
        return argZ;
    }
    ComplexNumber pow(double stepen){
        double a1 = Math.pow(length(),stepen)*(Math.cos(stepen*arg()));
        double b1 = Math.sin(stepen*arg());
        return new ComplexNumber(a1,b1);
    }
    boolean equals(ComplexNumber CN){
        if (a == CN.getA() && b == CN.getB()){
            return true;
        }else{
            return false;
        }
    }

}
