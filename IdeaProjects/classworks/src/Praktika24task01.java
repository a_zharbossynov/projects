import java.util.Scanner;

/**
 * Краткая самостоятельная на повторение из практики 24. Task-01.
 * Вычислить значение к-го члена арифметической прогрессиипо двум. Ее первым членам.
 */
public class Praktika24task01 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите a1: ");
        int a1 = sc.nextInt();
        System.out.println("Введите a2: ");
        int a2 = sc.nextInt();
        System.out.println("Введите k: ");
        int k = sc.nextInt();
        System.out.println(getNumber(a1, a2, k));
    }


    public static int getNumber(int a1, int a2, int k){
        int d = a2 - a1;
        int numK = a1 + (k-1) * d;
        return numK;
    }
}
