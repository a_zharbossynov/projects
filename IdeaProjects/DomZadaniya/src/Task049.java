import java.util.Scanner;

public class Task049 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите x и y для вектора: ");
        Vector2D v1 = new Vector2D(sc.nextDouble(), sc.nextDouble());
    }
}

class Vector2D{
    double x;
    double y;
    double number;

    Vector2D(){
        x = 0;
        y = 0;
    }

    Vector2D(double x, double y){
        this.x = x;
        this.y = y;
    }

    Vector2D add(Vector2D vector2D){
        double vector2D1x = x + vector2D.getX();
        double vector2D1y = y + vector2D.getY();
        return new Vector2D(vector2D1x,vector2D1y);
    }

    public double getX(){
        return x;
    }
    public double getY(){
        return y;
    }

    void add2(Vector2D vector2D){
        x = x + vector2D.getX();
        y = y + vector2D.getY();

    }

    Vector2D sub(Vector2D vector2D){
        double vector2D1x = x - vector2D.getX();
        double vector2D1y = y - vector2D.getY();
        return new Vector2D(vector2D1x,vector2D1y);
    }
    void sub2(Vector2D vector2D){
        x = x - vector2D.getX();
        y = y - vector2D.getY();
    }

    Vector2D mult(double number){
        x = x * number;
        y = y * number;
        return new Vector2D(x, y);
    }
    void mult2(Vector2D vector2D){
        x = x * vector2D.getX();
        y = y * vector2D.getY();
    }

    public String toString(){
        String s = "(" + x + "," + y +")";
        return s;
    }

    double length(Vector2D vector2D){
        double vectorLength = Math.sqrt(x*x+y*y);
        return vectorLength;
    }

    double scalarProduct(Vector2D vector2D){
        double vectorScalarProduct = ((x*vector2D.getX())+(y*vector2D.getY()));
        return vectorScalarProduct;
    }

    double cos(Vector2D vector2D){
        double vectorCos = scalarProduct(vector2D) / length(vector2D);
        return vectorCos;
    }

    boolean equals(Vector2D vector2D){
        if (x == vector2D.getX() && y == vector2D.getY()){
         return true;
        }else{
            return false;
        }
    }
}
