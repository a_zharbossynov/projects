package FactoryMethodOfCreatingMonsters;

/**
 * Created by MacBookAir on 02.03.17.
 */
abstract class Creator {
    public abstract Monster factoryMethod();
}
