import java.util.Scanner;
public class Task035 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите размер массива: ");
        int n = sc.nextInt();
        int massive[] = new int[n];
        System.out.println("Заполните массив: ");
        for (int i = 0; i < n; i++) {
            massive[i] = sc.nextInt();
        }
        for (int i = massive.length-1; i >= 0; i--) {
            for (int j = 0; j < i; j++){
                if (massive[j] > massive[j+1]){
                    int t = massive[j];
                    massive[j] = massive[j+1];
                    massive[j+1] = t;
                }
            }
        }
        System.out.println("Сортировка пузырьком: ");
        for (int i = 0; i < n; i++) {
            System.out.print(massive[i] + ", ");
        }
    }
}
