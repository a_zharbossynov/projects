package alghorithms.hw_03_05_2017;

/**
 * Created by MacBookAir on 03.05.17.
 */
public class Task3 {


    public static int numberOfCoins(int[] x, int money, int n){
        for (int i = 0; i < x.length; i++) {
            if ((money / x[i]) != 0) {
                if (((money / x[i]) == 1)) {
                money = money - x[i];
                n++;
                }else {
                n = n + (money / x[i]);
                money = money % x[i];
                }
            }
        }
        return n;
    }


    public static void main(String[] args) {
        int money = 99;                             // сумма сдачи
        int[] x = new int[]{50, 25, 10, 5, 1};      // номиналы монет(по убыванию)
        int n = 0;                                  // счетчик наименьшего кол-во монет

        System.out.println(numberOfCoins(x, money, n));
    }
}
