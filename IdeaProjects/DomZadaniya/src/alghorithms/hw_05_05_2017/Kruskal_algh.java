package alghorithms.hw_05_05_2017;

import java.util.Arrays;

/**
 *     1. Сортируем все ребра в неубывающем порядке их веса.
 *     2. Выбераем наименьшее ребро. Проверяем, не образует ли он цикл со сформированным остовным деревом.
 *         Если цикла нет, добавляем это ребро. Иначе отбрасываем.
 *     3. Повторяем (2) до тех пор, пока в остовном дереве не появятся (V-1) ребра.
 *
 *     Для обнаружения цикла во (2) используем Union-find alghorithm.
 */

public class Kruskal_algh {

    class Edge implements Comparable<Edge> {
        int src, end, weight;
        public int compareTo(Edge compareEdge) {
            return this.weight - compareEdge.weight;
        }
    }

    int V, E;
    Edge edge[];

    class subset {
        int parent, rank;
    }


    Kruskal_algh(int v, int e){
        V = v;
        E = e;
        edge = new Edge[E];
        for (int i = 0; i < e; ++i) {
            edge[i] = new Edge();
        }
    }


    public int find(subset subsets[], int i) {

        if (subsets[i].parent != i) {
            subsets[i].parent = find(subsets, subsets[i].parent);
        }

        return subsets[i].parent;
    }

    public void Union(subset subsets[], int x, int y) {
        int xroot = find(subsets, x);
        int yroot = find(subsets, y);

        if (subsets[xroot].rank < subsets[yroot].rank){
            subsets[xroot].parent = yroot;
        }
        else if (subsets[xroot].rank > subsets[yroot].rank){
            subsets[yroot].parent = xroot;
        }
        else
        {
            subsets[yroot].parent = xroot;
            subsets[xroot].rank++;
        }
    }


    public void kruskal(){
        Edge result[] = new Edge[V];

        for (int i = 0; i < V; ++i){
            result[i] = new Edge();
        }

        Arrays.sort(edge);

        subset subsets[] = new subset[V];

        for(int i = 0; i < V; ++i) {
            subsets[i] = new subset();
        }

        for (int v = 0; v < V; ++v) {
            subsets[v].parent = v;
            subsets[v].rank = 0;
        }

        int e = 0;
        int i = 0;

        while (e < V - 1) {
            Edge next_edge = edge[i++];

            int x = find(subsets, next_edge.src);
            int y = find(subsets, next_edge.end);

            if (x != y) {
                result[e++] = next_edge;
                Union(subsets, x, y);
            }
        }
        System.out.println("Наше минимальное остовное дерево содержащее: ");
        System.out.println();
        System.out.println("Ребра   Вес");
        for (i = 0; i < e; ++i)
            System.out.println(result[i].src + " - " + result[i].end + "    " + result[i].weight);
    }



    public static void main (String[] args) {
        int V = 6;
        int E = 10;
        Kruskal_algh graph = new Kruskal_algh(V, E);
        // a - 0; b - 1; c - 2; d - 3; e - 4; f - 5.

        // edge a-b
        graph.edge[0].src = 0;
        graph.edge[0].end = 1;
        graph.edge[0].weight = 3;

        // edge b-c
        graph.edge[1].src = 1;
        graph.edge[1].end = 2;
        graph.edge[1].weight = 1;

        // edge c-d
        graph.edge[2].src = 2;
        graph.edge[2].end = 3;
        graph.edge[2].weight = 6;

        // edge d-e
        graph.edge[3].src = 3;
        graph.edge[3].end = 4;
        graph.edge[3].weight = 8;

        // edge e-a
        graph.edge[4].src = 4;
        graph.edge[4].end = 5;
        graph.edge[4].weight = 6;

        // edge e-f
        graph.edge[5].src = 4;
        graph.edge[5].end = 5;
        graph.edge[5].weight = 2;


        // edge a-f
        graph.edge[6].src = 0;
        graph.edge[6].end = 5;
        graph.edge[6].weight = 5;

        // edge b-f
        graph.edge[7].src = 1;
        graph.edge[7].end = 5;
        graph.edge[7].weight = 4;

        // edge c-f
        graph.edge[8].src = 2;
        graph.edge[8].end = 5;
        graph.edge[8].weight = 4;

        // edge d-f
        graph.edge[9].src = 3;
        graph.edge[9].end = 5;
        graph.edge[9].weight = 5;

        graph.kruskal();
    }
}
