package ru.itis.kpfu.Model;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Group {

    private int id;
    private String title;

    //MARK: - Initializers
    public Group(ResultSet set) throws SQLException {
        this.id = set.getInt("id");
        this.title = set.getString("title");
    }
    public Group(int id, String title) {
        this.id = id;
        this.title = title;
    }

    //MARK: - Getters
    public int getId() {
        return id;
    }
    public String getTitle() {
        return title;
    }
}
