public class Ticket {
    private int number;

    Ticket(int number) {
        this.number = number;
    }

    public boolean getPalindrome() {
        String numberString = Integer.toString(number);
        char[] digits = numberString.toCharArray();
        boolean flag = true;
        for (int i = 0; i < digits.length/2 && flag; i++) {
            if (digits[i] != digits[digits.length - i - 1])
                flag = false;
        }
        return flag;
    }
    public boolean luckyMoscowTicket() {
        String numberString = Integer.toString(number);
        char[] digits = numberString.toCharArray();

        boolean flag = true;
        int sum1 = 0, sum2 = 0;
        if (digits.length % 2 != 0)
            flag = false;
        else {
            for (int i = 0; i < digits.length / 2; i++) {
                sum1 += digits[i];
                sum2 += digits[digits.length - i - 1];
            }
            if (sum1 != sum2) flag = false;
        }
        return flag;
    }

    public boolean luckyStPetersburgTicket() {
        String numberString = Integer.toString(number);
        char[] digits = numberString.toCharArray();

        boolean flag = true;
        int sum1 = 0, sum2 = 0;
        if (digits.length % 2 != 0)
            flag = false;
        else {
            for (int i = 0; i < digits.length; i += 2) {
                sum1 += digits[i];
                sum2 += digits[digits.length - i - 1];
            }
            if (sum1 != sum2) flag = false;
        }
        return flag;
    }

    public boolean halfLuckyMoscowTicket() {
        String numberString = Integer.toString(number);
        char[] digits = numberString.toCharArray();

        boolean flag = true;
        int sum1 = 0, sum2 = 0;
        if (digits.length % 2 != 0)
            flag = false;
        else {
            for (int i = 0; i < digits.length / 2; i++) {
                sum1 += digits[i];
                sum2 += digits[digits.length - i - 1];
            }
            if ((sum1 != (sum2 + 1)) && (sum1 != (sum2 - 1))) flag = false;
        }
        return flag;
    }

    public boolean halfLuckyStPetersburgTicket() {
        String numberString = Integer.toString(number);
        char[] digits = numberString.toCharArray();

        boolean flag = true;
        int sum1 = 0, sum2 = 0;
        if (digits.length % 2 != 0)
            flag = false;
        else {
            for (int i = 0; i < digits.length; i += 2) {
                sum1 += digits[i];
                sum2 += digits[digits.length - i - 1];
            }
            if ((sum1 != (sum2 + 1)) && (sum1 != (sum2 - 1))) flag = false;
        }
        return flag;
    }
}
