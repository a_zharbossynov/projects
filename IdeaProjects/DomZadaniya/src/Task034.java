import java.util.Scanner;
public class Task034 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int max = 0;
        System.out.println("Введите размер массива :");
        int n = sc.nextInt();
        int massive[] = new int[n];
        System.out.println("Заполните массив: ");
        for (int i = 0; i < n; i++){
            massive[i] = sc.nextInt();
        }
        for (int i = 0; i < n-2; i++){
            if ((massive[i] + massive[i+1] + massive[i+2]) > max){
                max = (massive[i] + massive[i+1] + massive[i+2]);
            }
        }
        System.out.println("Max = " + max);
    }
}
