/**
 * Практика-24. Заполнить очередь значениями арифметической прогрессии с заданными параметрам.
 * Решить первую задачу используя коллекции Java.
 */
import java.util.*;
public class Praktika24ex02 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите a1 и d: ");
        int a1 = sc.nextInt();
        int d = sc.nextInt();
        System.out.print("Введите n: ");
        int n = sc.nextInt();
        List list = new LinkedList();
        for (int i = 0; i < n; i++){
            list.add(a1+ d*i);
        }
        for (int i =0; i < list.size(); i++){
            System.out.println(list.get(i));
        }

    }
}

