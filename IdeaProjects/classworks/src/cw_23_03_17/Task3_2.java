package cw_23_03_17;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by MacBookAir on 23.03.17.
 *
 * Проверочная++
 * Задание номер 2.
 */
public class Task3_2 {
    public static List<String> getString(List<String> list, Function<String, String> func){
        return list.stream().map(func).collect(Collectors.toList());
    }
}
