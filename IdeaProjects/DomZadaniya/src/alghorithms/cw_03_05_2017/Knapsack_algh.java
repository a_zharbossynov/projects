package alghorithms.cw_03_05_2017;

/**
 * Created by MacBookAir on 03.05.17.
 */
public class Knapsack_algh {

    public static int max(int a, int b){
        return (a > b)? a : b;
    }


    public static int knapsack(int W, int weight[], int cost[], int n) {
        int K[][] = new int[n+1][W+1];
        for (int i = 0; i <= n; i++){
            for (int j = 0; j <= W; j++){
                if (i == 0 || j == 0)
                    K[i][j] = 0;
                else if (weight[i-1] <= j)
                    K[i][j] = max(cost[i-1] + K[i-1][j - weight[i-1]], K[i-1][j]);
                else
                    K[i][j] = K[i-1][j];
            }
        }
        return K[n][W];
    }

    public static void main(String[] args) {
        int cost[] = new int[]{12, 10, 20, 15};
        int weight[] = new int[]{2, 1, 3, 2};
        int W = 5;
        int n = cost.length;
        System.out.println(knapsack(W, weight, cost, n));
    }
}


