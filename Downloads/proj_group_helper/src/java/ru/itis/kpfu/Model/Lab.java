package ru.itis.kpfu.Model;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Lab {

    private int id;
    private String title;
    private boolean is_assigned;

    //MARK: - Initializers
    public Lab(ResultSet set) throws SQLException {
        this.id = set.getInt("id");
        this.title = set.getString("title");
    }

    //MARK: - Getters
    public int getId() {
        return id;
    }
    public String getTitle() {
        return title;
    }
    public boolean getIsAssigned(){
        return this.is_assigned;
    }

    //MARK: - Setters
    public void setAssigned(boolean assigned){
        this.is_assigned = assigned;
    }
}
