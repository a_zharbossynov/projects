package hw_02_03_2017.Pattern_Decorator;

public interface IArrayString {
    String toDisplay();
    String[] getArr();
}
