package ru.itis.kpfu.DAO;

import ru.itis.kpfu.Model.Group;

import java.util.ArrayList;

public interface GroupDAO {

    ArrayList<Group> getAll();
    Group getById(int id);
    String getNotice(int id_group);
    int getEditorId(int id);

    boolean setNotice(String notice, int id_group);
}
