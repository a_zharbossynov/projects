import java.util.Scanner;

/**
 * Краткая самостоятельная на повторение из практики 24. Task-03.
 * По заданному натуральному числу k вычислить сумму квадратов факториалов от 1 до k.
 */
public class Praktika24task03 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите k: ");
        int k = sc.nextInt();
        long sum = 0;
        for (int i = 1; i <= k; i++){
            long s = getFactorial(i);
            sum = sum + s;
        }
        System.out.println(sum);
    }


    public static long getFactorial(int k){
        if (k <= 1)
            return 1;
        else
            return k*k*getFactorial(k-1);
    }
}
