package cw_23_03_17;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Created by MacBookAir on 23.03.17.
 *
 * Проверочная++
 * Задание номер 4.
 */
public class Task4_2 {
    public static <T, K> List<K> getString(List<T> list, Function<T, K> func){
        return list.stream().map(func).collect(Collectors.toList());
    }
}
