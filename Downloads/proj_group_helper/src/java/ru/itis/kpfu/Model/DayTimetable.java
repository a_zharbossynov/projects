package ru.itis.kpfu.Model;

import java.util.Calendar;

public class DayTimetable {

    private int weekday;
    private Lesson[] lessons;
    private boolean is_current;

    //MARK: - Initializers
    public DayTimetable(int weekday){
        this.weekday = weekday;

        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        this.is_current = (day - 1) == weekday;

        lessons = new Lesson[6];
        for (int i = 0; i < lessons.length; i++) {
            lessons[i] = Lesson.getEmptyLesson(i);
        }
    }
    public void addLesson(Lesson lesson){
        if(lesson.getOrdinal() < this.lessons.length){
            int idx = lesson.getOrdinal();
            Lesson present = this.lessons[idx];

            if(present != null && !present.getIsEmpty()){
                this.lessons[idx] = present.merged(lesson);
            }else{
                this.lessons[idx] = lesson;
            }
        }
    }

    //MARK: - Getters
    public String getID(){
        switch (this.weekday){
            case 1:
                return "monday";
            case 2:
                return "tuesday";
            case 3:
                return "wednesday";
            case 4:
                return "thursday";
            case 5:
                return "friday";
            case 6:
                return "saturday";

            default:
                return "MISSINGNO";
        }
    }
    public String getName(){
        switch (this.weekday){
            case 1:
                return "Понедельник";
            case 2:
                return "Вторник";
            case 3:
                return "Среда";
            case 4:
                return "Четверг";
            case 5:
                return "Пятница";
            case 6:
                return "Суббота";

            default:
                return "MISSINGNO";
        }
    }
    public int getWeekday() {
        return weekday;
    }
    public Lesson[] getLessons(){
        return lessons;
    }
    public boolean isCurrent(){
        return this.is_current;
    }
}
