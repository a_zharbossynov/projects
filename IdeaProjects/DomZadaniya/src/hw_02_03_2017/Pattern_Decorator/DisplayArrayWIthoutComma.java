package hw_02_03_2017.Pattern_Decorator;


public class DisplayArrayWIthoutComma extends ArrayDisplayDecorator {
    private String [] arr;

    public DisplayArrayWIthoutComma(IArrayString basicDisplay){

        originalDisplay = basicDisplay;
    }

    public String[] getArr(){
        return arr;
    }

    public String toDisplay(){

        return  toDisplayWithoutComma(originalDisplay);
    }
    public String toDisplayWithoutComma(IArrayString display){

        return display.toDisplay().replaceAll(", ","");
    }
}
