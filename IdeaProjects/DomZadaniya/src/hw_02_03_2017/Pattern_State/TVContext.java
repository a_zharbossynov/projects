package hw_02_03_2017.Pattern_State;

/**
 * Created by MacBookAir on 06.03.17.
 */

public class TVContext implements IState {

    private IState tvState;

    public void setState(IState state) {
        this.tvState = state;
    }

    public IState getState() {
        return this.tvState;
    }

    @Override
    public void doAction() {
        this.tvState.doAction();
    }

}
