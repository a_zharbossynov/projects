<#-- @ftlvariable name="msg_error" type="java.lang.String" -->

<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">
    <title>Добро пожаловать!</title>

    <!-- Bootstrap core CSS + icons -->
    <link href="/resources/css/bootstrap.css" rel="stylesheet">
    <link href="/resources/css/offcanvas.css" rel="stylesheet">
    <link href="/resources/fonts/font-awesome.min.css" rel="stylesheet">

    <#--JavaScript-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>

</head>
<body>
<div><#include "element/header.ftl"></div>
<div class="container">
    <div class="row align-items-center">
        <div class="col-12">
            <div class="alert alert-light" role="alert">
                <h2 class="text-center">Казанский (Приволжский) федеральный университет – Высшая школа информационных
                    технологий и информационных систем</h2>
            </div>
            <div class="row align-items-center">
                <div class="col-6 mx-auto">
                    <h1 class="text-center">Вход на сайт</h1><br>
                    <h4 class="text-center" id="lbl_status" style="color: <#if msg_error??>orangered<#else>slategrey</#if>">
                    <#if msg_error??>ОШИБКА: ${msg_error}<#else>Введите свои логин и пароль</#if>
                    </h4><br>
                    <form action="/" method="post" id="form_login">
                        <div class="form-group">
                            <input type="text" class="form-control" placeholder="Логин" name="login" style="text-align: center" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" placeholder="Пароль" name="pass" style="text-align: center" required>
                        </div>
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Войти</button>
                    </form>
                </div>
            </div>
        </div><!--/span-->
    </div><!--/row-->

</div><!--/.container-->

<#--<script type="text/javascript">-->
    <#--jQuery( "#form_login" ).submit(function( event ) {-->

        <#--alert("jQuery is triggered, BTW");-->
        <#--var data = $("#form_login").serialize();-->

        <#--jQuery.post("/login/", data, function(response) {-->
            <#--var s_json = JSON.stringify(response);-->
            <#--var json = jQuery.parseJSON(s_json);-->

            <#--if(json.status === 0){-->
                <#--var lbl_status = jQuery("#lbl_status");-->
                <#--lbl_status.css("color", "red");-->
                <#--lbl_status.text(json.msg_error);-->
            <#--}-->
            <#--else if(json.status === 1){-->
                <#--window.location.assign("/profile");-->
            <#--}-->
            <#--else{-->
                <#--alert("Hm... Weird...")-->
            <#--}-->

        <#--}, "json").fail( function(jqXHR, textStatus){-->
            <#--alert(textStatus);-->
        <#--});-->

        <#--event.preventDefault();-->
    <#--});-->
<#--</script>-->

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="http://getbootstrap.com/assets/js/vendor/popper.min.js"></script>
<script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
<script src="http://getbootstrap.com/docs/4.0/examples/offcanvas/offcanvas.js"></script>
</body>
</html>