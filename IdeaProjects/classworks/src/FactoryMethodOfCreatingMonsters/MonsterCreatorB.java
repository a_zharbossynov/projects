package FactoryMethodOfCreatingMonsters;

/**
 * Created by MacBookAir on 02.03.17.
 */
public class MonsterCreatorB extends Creator {
    @Override
    public Monster factoryMethod() { return new MonsterB(); }
}

