//
//  UserStatusTableViewCell.swift
//  HomeWork3
//
//  Created by MacBookAir on 24.09.17.
//  Copyright © 2017 iOSlab. All rights reserved.
//

import UIKit

class UserStatusTableViewCell: UITableViewCell {

    @IBOutlet weak var userStatus: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
