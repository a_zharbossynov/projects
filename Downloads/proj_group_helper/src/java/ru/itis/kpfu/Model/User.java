package ru.itis.kpfu.Model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

public class User {

    private int id;
    private String login;
    private String email;
    private String fullname;
    private int id_group;

    //MARK: - Initializers
    public User(ResultSet set) throws SQLException {
        this.id = set.getInt("id");
        this.login = set.getString("login");
        this.email = set.getString("email");
        this.fullname = set.getString("fullname");
        this.id_group = set.getInt("id_group");
    }

    //MARK: - Getters
    public int getId() {
        return id;
    }
    public String getLogin() {
        return login;
    }
    public String getEmail() {
        return email;
    }
    public String getFullname() {
        return fullname;
    }
    public int getId_group() {
        return id_group;
    }

    //MARK: - Setters
    public void setEmail(String email) {
        this.email = email;
    }
    public void setFullname(String fullname) {
        this.fullname = fullname;
    }
    public void setId_group(int id_group) {
        this.id_group = id_group;
    }

    //MARK: - Convenience
    public HashMap<String, Object> getHashMap(){
        HashMap<String, Object> map = new HashMap<>();

        map.put("login", login);
        map.put("email", email);
        map.put("fullname", fullname);
        map.put("id_group", id_group);

        return map;
    }
}
