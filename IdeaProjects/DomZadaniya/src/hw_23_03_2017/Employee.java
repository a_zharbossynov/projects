package hw_23_03_2017;

/**
 * Created by MacBookAir on 26.03.17.
 */
public class Employee {
    private String name;
    private boolean rich;
    private boolean happy;
    private boolean compitent;

    public Employee(String name, boolean rich, boolean happy, boolean compitent){
        this.name = name;
        this.rich = rich;
        this.happy = happy;
        this.compitent = compitent;
    }

    public boolean isRich() {
        return rich;
    }

    public boolean isHappy() {
        return happy;
    }

    public boolean isCompitent() {
        return compitent;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString(){
        return getName();
    }
}
