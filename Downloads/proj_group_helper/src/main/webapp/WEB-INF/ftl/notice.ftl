<#-- @ftlvariable name="fullname" type="java.lang.String" -->
<#-- @ftlvariable name="login" type="java.lang.String" -->
<#-- @ftlvariable name="notice" type="java.lang.String" -->
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">
    <title>Редактирование объявления</title>

    <!-- Bootstrap core CSS + icons -->
    <link href="/resources/css/bootstrap.css" rel="stylesheet">
    <link href="/resources/css/offcanvas.css" rel="stylesheet">
    <link href="/resources/fonts/font-awesome.min.css" rel="stylesheet">

<#--JavaScript-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>

</head>

<body>

<nav class="navbar navbar-expand-md fixed-top navbar-dark" style="background-color: #0c2954;">
    <a class="navbar-brand" href="#">Group helper</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler"
            aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarToggler">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <div class="d-md-none d-lg-none d-xl-none">
                <li class="nav-item">
                    <a class="nav-link" href="/schedule/">Расписание <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/profile/">Профиль</a>
                </li>
            </div>
        </ul>
        <a href="/logout/" class="btn btn-outline-light" role="button">Выйти</a>
    </div>
</nav>

<div class="container">

    <div class="row row-offcanvas row-offcanvas-right">
        <div class="col-12 col-md-9">
            <h1>Объявление</h1><br>
            <form action="/notice/" method="post">
                <div class="form-group">
                    <label for="notice_text">Введите ваше объявление, оно высветится справа сбоку у всех одногруппников</label>
                    <textarea class="form-control" id="txt_notice" name="notice" rows="5" placeholder="Введите текст..."><#if notice??>${notice}</#if></textarea>
                </div>
                <button type="submit" class="btn btn-primary">Сохранить</button>
            </form>
        </div><!--/span-->

        <div class="col-6 col-md-3 sidebar-offcanvas" id="sidebar">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Привет, ${fullname}!</h4>
                </div>
                <div class="list-group">
                    <a href="/schedule/" class="list-group-item">Расписание</a>
                    <a href="/profile/" class="list-group-item">Профиль</a>
                </div>
            </div>
            <br>
            <#if notice??>
                <div class="card">
                    <div class="card-body">
                        <h4 class="card-title">Объявление</h4>
                        <p class="card-text">${notice}</p>
                    </div>
                </div>
            </#if>
        </div><!--/span-->
    </div><!--/row-->
</div><!--/.container-->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="http://getbootstrap.com/assets/js/vendor/popper.min.js"></script>
<script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
<script src="http://getbootstrap.com/docs/4.0/examples/offcanvas/offcanvas.js"></script>
</body>
</html>
