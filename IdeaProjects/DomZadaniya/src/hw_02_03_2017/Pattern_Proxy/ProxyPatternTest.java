package hw_02_03_2017.Pattern_Proxy;

/**
 * Created by MacBookAir on 06.03.17.
 */


public class ProxyPatternTest {

    public static void main(String[] args){
        ICommandExecutor executor = new CommandExecutorProxy("Pankaj", "wrong_pwd");
        try {
            executor.runCommand("ls -ltr");
            executor.runCommand(" rm -rf abc.pdf");
        } catch (Exception e) {
            System.out.println("Exception Message::"+e.getMessage());
        }

    }

}
