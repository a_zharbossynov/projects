package ru.itis.kpfu.Model;

import org.json.JSONObject;

public class UserBlueprint {

    private String fullname;
    private String login;
    private String email;
    private String password;
    private int id_group = -1;

    //MARK: - Initializers
    public UserBlueprint(){}

    //MARK: Getters
    public String getFullname() {
        return fullname;
    }
    public String getLogin() {
        return login;
    }
    public String getEmail() {
        return email;
    }
    public String getPassword() {
        return password;
    }
    public int getId_group() {
        return id_group;
    }

    //MARK: - Setters
    public void setFullname(String fullname) {
        this.fullname = fullname;
    }
    public void setLogin(String login) {
        this.login = login;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public void setId_group(int id_group) {
        this.id_group = id_group;
    }

    //MARK: - Checking
    public boolean isValid(){
        return isFullnameValid() && isLoginValid() && isEmailValid() && isPasswordValid();
    }
    private boolean isFullnameValid(){
        return true;
    }
    private boolean isLoginValid(){
        return true;
    }
    private boolean isEmailValid(){
        return true;
    }
    private boolean isPasswordValid(){
        return true;
    }

    //MARK: - Extracting
    public String getValuesString(){
        JSONObject json = new JSONObject();
        json.put("fullname", fullname);
        json.put("login", login);
        json.put("email", email);
        json.put("password", password);

        if(this.id_group != -1){
            json.put("id_group", id_group);
        }

        return json.toString();
    }
}
