package Exam_1sem;
import java.math.*;
import java.lang.*;

/**
 * Created by MacBookAir on 27.12.16.
 */
public class Ex04 {
    public static void main(String[] args) {
        System.out.println("Совершенным называется натуральное число, равное сумме своих делителей");
        BigInteger b1 = BigInteger.valueOf(2);
        BigInteger a1 = b1.pow(19936);
        BigInteger b2 = BigInteger.valueOf(2);
        BigInteger a2 = b2.pow(19937);
        int s = a2.intValue();
        int p = a1.intValue();

        int num = p * (s - 1);
        int sum = 0;
        int k = 0;
        for (int i = 1; i < num; i++){
            if (num % i == 0){
                k = i;
                sum = k + sum;
            }
        }
        if (num == sum) {
            System.out.println(sum + " - совершенное число");}
        else {
            System.out.println("Не совершенное число");
        }

    }
}
