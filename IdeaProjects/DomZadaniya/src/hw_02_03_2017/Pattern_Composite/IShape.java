package hw_02_03_2017.Pattern_Composite;

/**
 * Created by MacBookAir on 06.03.17.
 */
public interface IShape {
    public void draw(String fillColor);
}
