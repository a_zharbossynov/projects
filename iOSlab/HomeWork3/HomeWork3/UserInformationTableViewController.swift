//
//  UserInformationTableViewController.swift
//  HomeWork3
//
//  Created by MacBookAir on 24.09.17.
//  Copyright © 2017 iOSlab. All rights reserved.
//

import UIKit

class UserInformationTableViewController: UITableViewController {
    
    let mainInfo = ["7 october", "Software Developer", "have a harem", "English, 한국어", "Elon Musk, Bill Gates"]
    
    let contacts = ["+1 (980) 3434040", "Mars, R2D2 str.", "linkedin.com/meeermaaaiid"]
    
    let career = ["Sony corp. 2012-2016", "Apple Inc. 2016-..."]
    
    let education = ["Stanford University 2008-2012"]
    
    let gifts = [#imageLiteral(resourceName: "imgGifts1"),#imageLiteral(resourceName: "imgGifts3"),#imageLiteral(resourceName: "imgGifts4"),#imageLiteral(resourceName: "imgGifts2"),#imageLiteral(resourceName: "imgGifts5")]
    
    let others = ["Interesting pages" , "Notes", "Documents"]
    
    @IBOutlet weak var userNameSurname2: UILabel!
    
    @IBOutlet weak var webStatus2: UILabel!
    
    @IBOutlet weak var userAge2: UILabel!
    
    @IBOutlet weak var location2: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 6
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
            return mainInfo.count
        }
        else if section == 1 {
            return contacts.count
        }
        else if section == 2 {
            return career.count
        }
        else if section == 3 {
            return education.count
        }
        else if section == 4 {
            return gifts.count
        }
        else {
            return others.count
        }
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        
        if indexPath.section == 0 {
            cell.textLabel?.text = mainInfo[indexPath.row]
        }
        else if indexPath.section == 1 {
            cell.textLabel?.text = contacts[indexPath.row]
        }
        else if indexPath.section == 2 {
            cell.textLabel?.text = career[indexPath.row]
        }
        else if indexPath.section == 3 {
            cell.textLabel?.text = education[indexPath.row]
        }
        else if indexPath.section == 4 {
            cell.imageView?.image = gifts[indexPath.row]
        }
        else {
            cell.textLabel?.text = others[indexPath.row]
        }

        // Configure the cell...

        return cell
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
//        if section == 0 {
//            return ""
//        }
//        else if section == 1 {
//            return "CONTACTS"
//        }
//        else if section == 2{
//            return "CAREER"
//        }
//        else if section == 3{
//            return "EDUCATION"
//        }
//        else if section == 4{
//            return "GIFTS"
//        }
//        else {
//            return ""
//        }
        
        switch section {
        case 0:
            return ""
        case 1:
            return "CONTACTS"
        case 2:
            return "CAREER"
        case 3:
            return "EDUCATION"
        case 4:
            return "GIFTS"
        case 5:
            return ""
        default:
            return ""
        }
        
        
        
    }
    
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
