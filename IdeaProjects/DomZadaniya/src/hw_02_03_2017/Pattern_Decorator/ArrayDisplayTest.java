package hw_02_03_2017.Pattern_Decorator;
import java.util.Arrays;


public class ArrayDisplayTest {
    public static void main(String [] args){
        String [] arr = new String[]{"Dog", "Cat", "Bird", "Fish"};
        IArrayString one = new SimpleDisplayString(arr);
        System.out.println(Arrays.toString(arr));
        IArrayString[] array = new IArrayString[]{one,
                new DisplayArrayWIthoutComma(one),
                new DisplayArraySort(one),
                new DisplayArraySpace(one)};

        for(IArrayString x:array){
            System.out.print(x.toDisplay());
            System.out.println();
        }

    }
}
