package hw_21_02_2017;

/**
 * Created by MacBookAir on 27.02.17.
 */
public class CalculatorDemo {
    public static void main(String[] args) {
        if (args.length >= 3) {
            new Calculator(Double.valueOf(args[0]), args[1], Double.valueOf(args[2]));
        } else {
            System.out.println("Enter your parameters like this: 1 + 1" );
            return;
        }
    }
}
