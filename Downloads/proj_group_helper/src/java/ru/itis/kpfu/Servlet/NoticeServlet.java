package ru.itis.kpfu.Servlet;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import ru.itis.kpfu.Model.Group;
import ru.itis.kpfu.Model.User;
import ru.itis.kpfu.Service.GroupService;
import ru.itis.kpfu.Service.Templater;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "NoticeServlet", urlPatterns = {"/notice/*"})
public class NoticeServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        ProfileServlet.ensureVariables(req, (exist, user, service) -> {
            if(exist){
                GroupService groupService = new GroupService();
                int editorId = groupService.getEditorId(user.getId_group());

                if(editorId == user.getId()){
                    this.displayContent(req, resp, user);
                }
            }else{
                req.getSession().invalidate();
                resp.sendRedirect(LoginServlet.LINK_ERROR_ACCESS);
            }
        });
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String notice = req.getParameter("notice");
        ProfileServlet.ensureVariables(req, (exist, user, service) -> {
            if(exist){
                GroupService groupService = new GroupService();
                if(groupService.setNotice(notice, user.getId_group())){
                    resp.sendRedirect("/profile");
                }else{
                    this.displayContent(req, resp, user);
                }
            }else{
                req.getSession().invalidate();
                resp.sendRedirect(LoginServlet.LINK_ERROR_ACCESS);
            }
        });
    }

    private void displayContent(HttpServletRequest req, HttpServletResponse resp, User user) throws IOException {
        ServletContext context = req.getServletContext();
        Template templ = Templater.getTemplate(context, "notice.ftl");

        resp.setCharacterEncoding("UTF-8");
        PrintWriter writer = resp.getWriter();
        Map<String, Object> data = new HashMap<>();
        data.putAll(user.getHashMap());

        GroupService service = new GroupService();
        String notice = service.getNotice(user.getId_group());
        data.put("notice", notice);

        try {
            templ.process(data, writer);
        } catch (TemplateException e) {
            System.out.println("No templates were found(processed)");
            e.printStackTrace();
        }

        writer.flush();
        writer.close();
    }
}
