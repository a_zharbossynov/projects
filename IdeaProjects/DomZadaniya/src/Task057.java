public class Task057 {
    public static void main(String[] args) {
    }
}

class RationalComplexNumber extends RationalFraction{
    RationalFraction a, b;
    RationalComplexNumber(){
        a.numerator = 0;
        b.numerator = 0;
    }

    RationalComplexNumber(RationalFraction a, RationalFraction b){
        this.a = a;
        this.b = b;
    }

    public RationalFraction getA() {
        return a;
    }

    public RationalFraction getB() {
        return b;
    }

    RationalComplexNumber add(RationalComplexNumber rcnum){
        RationalFraction rcnumA = a.add(rcnum.getA());
        RationalFraction rcnumB = b.add(rcnum.getB());
        return new RationalComplexNumber(rcnumA, rcnumB);
    }

    RationalComplexNumber sub(RationalComplexNumber rcnum){
        RationalFraction rcnumA = a.sub(rcnum.getA());
        RationalFraction rcnumB = b.sub(rcnum.getB());
        return new RationalComplexNumber(rcnumA, rcnumB);
    }

    RationalComplexNumber mult(RationalComplexNumber rcnum){
        RationalFraction rcnumA = a.mult(rcnum.getA());
        RationalFraction rcnumB = b.mult(rcnum.getB());
        return new RationalComplexNumber(rcnumA, rcnumB);
    }

    public String toString(){
        String s = a.toString() + "+" + "(" + b.toString() + ")";
        return s;
    }
}
