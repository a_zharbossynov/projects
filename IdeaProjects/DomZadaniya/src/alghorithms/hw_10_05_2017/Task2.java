package alghorithms.hw_10_05_2017;


public class Task2 {

    public static class Graph {
        int V;
        Stack stack;
        int[][] matrix;
        int[] vertices;
        int[] distances;
        int[] previous;
        boolean[] visited;


        public Graph(int V) {
            this.V = V;
            vertices = new int[V];
            visited = new boolean[V];
            previous = new int[V];
            distances = new int[V];
            matrix = new int[V][V];
            stack = new Stack(V);
            for (int i = 0; i < V; i++) {              // Инициализируем расстояния до всех вершин как минус бесконечность
                addVertex(i);                          // и расстояние до источника(source) как 0,
                distances[i] = Integer.MIN_VALUE;      // затем находим топологическую сортировку графика.
                previous[i] = -1;
            }
        }

        public void addVertex(int name) {
            vertices[name] = name;
        }

        public void addEdge(int src, int end, int weight) {
            matrix[src][end] = weight;
        }

        // Выполняем следующие действия для каждой вершины в топологическом порядке.
        // if (dist[v] < dist[u] + weight(u, v))
        //    dist[v] = dist[u] + weight(u, v)
        public void maxDistances() {
            while (!stack.isEmpty()) {
                int src = stack.pop();
                if (distances[src] != Integer.MIN_VALUE) {
                    for (int neighbor = 0; neighbor < V; neighbor++) {
                        if (matrix[src][neighbor] != 0) {
                            if (distances[neighbor] < distances[src] + matrix[src][neighbor]) {
                                previous[neighbor] = src;
                                distances[neighbor] = distances[src] + matrix[src][neighbor];
                            }
                        }
                    }
                }
            }
        }

        // Получив топологический порядок,
        // по очереди обрабатываем все вершины в топологическом порядке.
        // Для каждой обработанной вершины обновляем расстояния ее смежности, используя расстояние текущей вершины.
        public void topologicalSort() {
            for (int i = 0; i < V; i++) {
                if (!visited[i]) {
                    depthFirstSearch(i);
                }
            }
        }

        // Mетод обхода графа. (алгоритм поиска в глубину)
        // Для каждой не пройденной вершины необходимо найти все не пройденные смежные вершины
        // и повторить поиск для них.
        // И когда вершина обработана, заносим ее в стек.
        public void depthFirstSearch(int src) {
            visited[src] = true;
            for (int neighbor = 0; neighbor < V; neighbor++) {
                if (matrix[src][neighbor] != 0 && !visited[neighbor]) {
                    depthFirstSearch(neighbor);
                }
            }
            stack.push(src);
        }

        public void printLongestDistance(int src) {
            int max = 0;
            for (int j = 0; j < V; j++) {
                int distance = distances[j];
                if (distance > max) {
                    max = distance;
                }
            }
            System.out.println("Длина cамого длинного пути в ациклическом ориентированном взвешанном графе: " + max);
        }

        public void longPath(int src) {
            topologicalSort();
            distances[src] = 0;
            maxDistances();
            printLongestDistance(src);

        }

    }

    public static class Stack {
        int maxSize;
        int[] stack;
        int top = -1;
        int size = 0;

        public Stack(int maxSize) {
            this.maxSize = maxSize;
            stack = new int[maxSize];
        }

        //Помещает элемент в вершину стека.
        public void push(int item) {
            stack[++top] = item;
            size++;
        }

        //Извлекает верхний элемент удаляя его из стека.
        public int pop() {
            int item = stack[top--];
            size--;
            return item;
        }


        public boolean isEmpty() {
            return size == 0;
        }
    }

    public static void main(String[] args) {
        Graph g = new Graph(4);

        // добавлем ориентированные ребра(исток, сток, вес)
        g.addEdge(0, 1, 5);
        g.addEdge(0, 2, 7);
        g.addEdge(0, 3, 6);
        g.addEdge(1, 0, -5);
        g.addEdge(1, 2, 16);
        g.addEdge(2, 0, -7);
        g.addEdge(2, 1, -16);
        g.addEdge(2, 3, 3);
        g.addEdge(3, 0, -6);
        g.addEdge(3, 2, -3);

        int source = 0;
        g.longPath(source);  // задаем источник для самого длинного пути
    }
}