/**
 * Created on 01.12.16.
 * Task-1
 */

import java.util.Scanner;
public class Task1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Write 'year' from 1 to 9999:");
        int year = sc.nextInt();
        if (year > 0 && year < 10000){
            System.out.println(getYear(year));
        }else{
            System.out.println(" 'Year' must be from 1 to 9999!");
        }


    }

    public static String getYear(int year){            // Дата празднования дня программиста!
        String message;
        if (((year % 400 == 0) || (year % 4 == 0)) && (year % 100 != 0)){
            message = "12/09/" + year;
        }else{
            message = "13/09/" + year;
        }
        return message;
    }
}
