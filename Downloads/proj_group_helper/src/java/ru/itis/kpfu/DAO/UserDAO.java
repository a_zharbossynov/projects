package ru.itis.kpfu.DAO;

import ru.itis.kpfu.Model.User;
import ru.itis.kpfu.Model.UserBlueprint;

import java.util.ArrayList;

public interface UserDAO {

    //Retrieving
    ArrayList<User> getAll();
    ArrayList<User> getGroup(int id);
    User getByLogin(String login);
    User getByID(int id);

    //Checking
    User auth(String login, String pass);
    boolean checkLoginAvailable(String login);
    boolean checkEmailAvailable(String email);

    //Managing
    int add(UserBlueprint user);
    boolean remove(int id);

    //Updating
    boolean updatePassword(String pass, int id);
    boolean updateFullname(String fullname, int id);
    boolean updateEmail(String email, int id);
    boolean updateGroup(int id_group, int id);
    boolean updateLab(int id_lab, boolean joined, int id);
}
