package FactoryMethodOfCreatingMonsters;

/**
 * Created by MacBookAir on 02.03.17.
 */
public class FactoryMethodMonsters {
    public static void main(String[] args) {
        // an array of creators
        Creator[] creators = {new MonsterCreatorA(), new MonsterCreatorB()};
        // iterate over creators and create monsters
        for (Creator creator: creators) {
            Monster m = creator.factoryMethod();
            System.out.printf("Created {%s}\n", m.getClass());
        }
    }


}
