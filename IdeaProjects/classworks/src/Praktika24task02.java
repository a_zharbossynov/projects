/**
 * Краткая самостоятельная на повторение из практики 24. Task-02.
 * Возвести вещественное число n в целую степень k.
 * Используйте алгоритм быстрого возведения в степень - бинарный/справа- налево)
 */
import java.util.Scanner;
public class Praktika24task02 {
        public static void main(String[] args) {
            Scanner sc = new Scanner(System.in);
            System.out.println("Введите вещественное число n и степень k: ");
            System.out.println(getDegree(sc.nextDouble(), sc.nextInt()));
        }

        public static double getDegree(double a, int n) {
            if (n == 0)
                return 1;
            if (n % 2 == 1)
                return getDegree (a, n-1) * a;
            else {
                double b = getDegree (a, n/2);
                return b * b;
            }
        }
}
