package hw_21_02_2017;

/**
 * Created by MacBookAir on 27.02.17.
 */
public class ExtraFunctions implements IExtraFunctions {

    public ExtraFunctions(double a, String sym) {
        if (sym.equals("square")) {
            System.out.println(a + " ^2 " + " = " + square(a));
            return;
        }
        if (sym.equals("sqRoot")) {
            System.out.println(a + " ^(1/2) " + " = " + sqRoot(a));
            return;
        }
        if (sym.equals("cos")) {
            System.out.println(cosinus(a));
            return;
        }
        if (sym.equals("sin")) {
            System.out.println(sinus(a));
            return;
        }
        System.out.println("Unknown operation");
    }

    public double square(double a){
        return a*a;
    }
    public double sqRoot(double a){
        return Math.sqrt(a);
    }
    public double cosinus(double a){
        return Math.cos(a);
    }
    public double sinus(double a){
        return Math.sin(a);
    }

}
