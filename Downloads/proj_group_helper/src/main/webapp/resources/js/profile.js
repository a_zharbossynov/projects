var pass_first;
var pass_secnd;

jQuery("#fld_pass_first").on('input', function(){
    pass_first = jQuery(this).val();
    updatePassFields();
});
jQuery("#fld_pass_secnd").on('input', function(){
    pass_secnd = jQuery(this).val();
    updatePassFields();
});

function updatePassFields(empty) {

    var fld_first = jQuery("#fld_pass_first");
    var fld_secnd = jQuery("#fld_pass_secnd");

    if(empty){
        fld_first.css("border", "1px solid light-gray");
        fld_secnd.css("border", "1px solid light-gray");
        return;
    }

    if(pass_first !== pass_secnd){
        fld_secnd.css("border", "1px solid red");
        fld_first.css("border", "1px solid red");
    }else{
        if(pass_first !== null && pass_first !== '' && pass_first.length >= 6 && pass_first.length <= 16){
            fld_first.css("border", "1px solid #7ded0e");
            fld_secnd.css("border", "1px solid #7ded0e");
        }else{
            updatePassFields(true);
        }
    }
}