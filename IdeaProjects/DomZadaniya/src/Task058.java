public class Task058 {
    public static void main(String[] args) {
    }
}

class RationalComplexVector2D extends RationalComplexNumber{
    RationalComplexNumber x, y;

    RationalComplexVector2D(){
        x.numerator = 0;
        y.numerator = 0;
    }

    RationalComplexVector2D(RationalComplexNumber x, RationalComplexNumber y){
        this.x = x;
        this.y = y;
    }

    public RationalComplexNumber getX() {
        return x;
    }

    public RationalComplexNumber getY() {
        return y;
    }

    RationalComplexVector2D add(RationalComplexVector2D vector){
        RationalComplexNumber vectorX = x.add(vector.getX());
        RationalComplexNumber vectorY = y.add(vector.getY());
        return new RationalComplexVector2D(vectorX, vectorY);
    }

    public String toString(){
        String s = "(" + x.toString() + ";" + y.toString() + ")";
        return s;
    }
    RationalComplexNumber scalarProduct(RationalComplexVector2D vector){
        RationalComplexNumber scProduct = (x.mult(vector.getX())).add(y.mult(vector.getY()));
        return scProduct;
    }

}