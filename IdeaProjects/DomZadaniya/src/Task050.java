import java.util.Scanner;

public class Task050 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите числитель дроби: ");
        int num = sc.nextInt();
        System.out.println("Введите знаменатель дроби: ");
        int den = sc.nextInt();

        RationalFraction fraction = new RationalFraction(num, den);
        fraction.reduce();
        System.out.println(fraction.getNumerator());
        System.out.println(fraction.getDenominator());
    }
}

    class RationalFraction {
    int numerator;     // числитель(делимое)
    int denominator;   // знаменатель(делитель)

    RationalFraction(){
        numerator = 0;
    }

    RationalFraction(int num, int den){
        numerator = num;
        denominator = den;
    }
    void reduce(){    // сокращение дроби
        int n = gcd(numerator, denominator);
        numerator = numerator / n;
        denominator = denominator / n;
    }
    public static int gcd(int numerator, int denominator){
        while (denominator != 0){
            int tmp = numerator % denominator;
            numerator = denominator;
            denominator = tmp;
        }
        return numerator;
    }
    public int getNumerator(){
        return numerator;
    }
    public int getDenominator(){
        return denominator;
    }
    RationalFraction add(RationalFraction RF){
        int RF1numerator = (gcd(numerator, denominator) / denominator)*numerator + (gcd(RF.getNumerator(), RF.getDenominator()) / RF.getDenominator())*RF.getNumerator();
        int RF1denominator = gcd(numerator, denominator);
        RationalFraction ratF = new RationalFraction(RF1numerator,RF1denominator);
        ratF.reduce();
        return ratF;
    }
    RationalFraction sub(RationalFraction RF){
        int RF1numerator = (gcd(numerator, denominator) / denominator)*numerator - (gcd(RF.getNumerator(), RF.getDenominator()) / RF.getDenominator())*RF.getNumerator();
        int RF1denominator = gcd(numerator, denominator);
        RationalFraction ratF = new RationalFraction(RF1numerator,RF1denominator);
        ratF.reduce();
        return ratF;
        }
    void sub2(RationalFraction RF){
        numerator = (gcd(numerator, denominator) / denominator)*numerator - (gcd(RF.getNumerator(), RF.getDenominator()) / RF.getDenominator())*RF.getNumerator();
        denominator = gcd(numerator, denominator);
        RationalFraction ratF = new RationalFraction(numerator, denominator);
        ratF.reduce();
    }
    RationalFraction mult(RationalFraction RF){
        int RF1numerator = numerator * RF.getNumerator();
        int RF1denominator = denominator * getDenominator();
        RationalFraction ratF = new RationalFraction(RF1numerator,RF1denominator);
        ratF.reduce();
        return ratF;
    }
    void mult2(RationalFraction RF){
        numerator = numerator * RF.getNumerator();
        denominator = denominator * getDenominator();
    }
    RationalFraction div(RationalFraction RF){
        int RF1numerator = numerator * RF.getDenominator();
        int RF1denominator = denominator * RF.getNumerator() ;
        RationalFraction ratF = new RationalFraction(RF1numerator,RF1denominator);
        ratF.reduce();
        return ratF;
    }
    void div2(RationalFraction RF){
        numerator = numerator * RF.getDenominator();
        denominator = denominator * RF.getNumerator();
        RationalFraction ratF = new RationalFraction();
        ratF.reduce();
    }
    public String toString(){
            String s = numerator + "/" + denominator;
        return s;
    }
    double value(){
        double m = numerator / denominator;
        return m;
    }
    boolean equals(RationalFraction RF){
        if(numerator == RF.getNumerator() && denominator == RF.getDenominator()){
            return true;}
        else{
            return false;
        }
    }
    int numberpart(){
       int k = numerator / denominator;
        return k;
    }

}

