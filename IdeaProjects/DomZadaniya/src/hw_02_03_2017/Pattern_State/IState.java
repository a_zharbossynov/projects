package hw_02_03_2017.Pattern_State;

/**
 * Created by MacBookAir on 06.03.17.
 */

public interface IState {
    public void doAction();
}
