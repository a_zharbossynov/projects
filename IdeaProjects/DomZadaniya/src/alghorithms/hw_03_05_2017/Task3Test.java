package alghorithms.hw_03_05_2017;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

/**
 * Created by MacBookAir on 03.05.17.
 */
public class Task3Test {
    @Test
    public void numberOfCoins() throws Exception {
        int money = 99;
        int[] x = new int[]{50, 25, 10, 5, 1};
        int n = 0;
        int exp = 8;
        assertEquals(exp, Task3.numberOfCoins(x, money, n));
    }

}