package cw_23_03_17;

import java.util.Arrays;

/**
 * Created by MacBookAir on 23.03.17.
 *
 * Проверочная
 * Задание номер 1.
 */

public class Task1 {
    public static void main(String [] args){

        String[] names = new String[] {"Kevin" ,"Lee" ,"Adrianne" , "Samsung" , "ITIS"};

        Arrays.stream(names).sorted((n1, n2) -> n2.length() - n1.length())
                .forEach((String value) -> System.out.println(value));


        System.out.println("-------------------------------------");


        Arrays.stream(names).sorted((n1, n2) -> n1.length() - n2.length())
                .forEach((String value) -> System.out.println(value));


        System.out.println("-------------------------------------");


        Arrays.stream(names).sorted((n1, n2) -> n1.compareTo(n2))
                .forEach((String value) -> System.out.println(value));


        System.out.println("-------------------------------------");

            Arrays.stream(names).sorted((n1, n2) -> n1.equals("ITIS") ? -1 : n1.compareTo(n2))
                    .forEach((String value) -> System.out.println(value));


        System.out.println("-------------------------------------");


        Arrays.stream(names).sorted(Task1::comp1).forEach(n -> System.out.print(" " + n));
        System.out.println();
        Arrays.stream(names).sorted(Task1::comp2).forEach(n -> System.out.print(" " + n));
        System.out.println();

    }


    public static int comp1(String n1, String n2) {
        if(n1.length() < n2.length())
            return 1;
        else if(n1.length() > n2.length()){
            return -1;
        }
        else return 0;
    }

    public static int comp2(String n1, String n2) {
        if(n1.length() < n2.length())
            return -1;
        else if(n1.length() > n2.length()){
            return 1;
        }
        else return 0;
    }

    // method 3


    // method 4


}


