/**
 * @author Alisher Zharbossynov
 * 11-602
 * Task028
 */
import java.util.Scanner;
public class Task028 {
    public static void main(String[] args) {
        Scanner task28 = new Scanner(System.in);
        System.out.println("Введите k и m: ");
        int k = task28.nextInt();
        int m = task28.nextInt();
        for (int i = k+1; i < m; i++){
            if (i % 3 == 0){
                System.out.println(i);
            }
        }
    }
}
