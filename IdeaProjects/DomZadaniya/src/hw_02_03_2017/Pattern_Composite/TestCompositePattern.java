package hw_02_03_2017.Pattern_Composite;

/**
 * Created by MacBookAir on 06.03.17.
 */
public class TestCompositePattern {
    public static void main(String[] args) {
        IShape tri = new Triangle();
        IShape tri1 = new Triangle();
        IShape cir = new Circle();

        Drawing drawing = new Drawing();
        drawing.add(tri1);
        drawing.add(tri1);
        drawing.add(cir);

        drawing.draw("Red");

        drawing.clear();

        drawing.add(tri);

        drawing.add(cir);
        drawing.draw("Green");
    }
}
