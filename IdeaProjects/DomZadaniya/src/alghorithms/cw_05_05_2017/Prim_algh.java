package alghorithms.cw_05_05_2017;


public class Prim_algh {
    public static void main(String[] args) {
        Prim_algh t = new Prim_algh();
        int graph[][] = new int[][] {{0, 3, 0, 0, 6, 5},
                                     {3, 0, 1, 0, 0, 4},
                                     {0, 1, 0, 6, 0, 4},
                                     {0, 0, 6, 0, 8, 5},
                                     {0, 0, 0, 8, 0, 2},
                                     {5, 4, 4, 5, 2, 0},
        };

        t.prim(graph);

    }

    int vertix = 6;
    final int INF = 1000 * 1000 * 1000;

    public int minVertixValue(int value[], Boolean Set[]) {
        int min = INF, min_index = -1;

        for (int i = 0; i < vertix; i++)
            if (Set[i] == false && value[i] < min) {
                min = value[i];
                min_index = i;
            }
        return min_index;
    }


    public void prim(int graph[][]) {
        int parent[] = new int[vertix];
        int key[] = new int [vertix];
        Boolean Set[] = new Boolean[vertix];

        for (int i = 0; i < vertix; i++) {
            key[i] = INF;
            Set[i] = false;
        }

        key[0] = 0;
        parent[0] = -1;

        for (int count = 0; count < vertix - 1; count++) {
            int u = minVertixValue(key, Set);
            Set[u] = true;

            for (int v = 0; v < vertix; v++) {

                if (graph[u][v] != 0 && Set[v] == false && graph[u][v] < key[v]) {
                    parent[v] = u;
                    key[v] = graph[u][v];
                }
            }
        }

        printTable(parent, vertix, graph);
    }

    public void printTable(int parent[], int vertix, int graph[][]) {
        int sum = 0;
        System.out.println("Ребра   Вес");
        for (int i = 1; i < vertix; i++) {
            System.out.println(parent[i] + " - " + i + "    " + graph[i][parent[i]]);
            sum = sum + graph[i][parent[i]];
        }
        System.out.println();
        System.out.println("Сумма всех ребер равна: " + sum);
    }



}
