package Exam_1sem;

import java.util.Scanner;

/** Exercise 20!!! Находит всевозможные комбинации слагаемых.
 * Created by MacBookAir on 27.12.16.
 */

public class Ex20{
    static int[] massive = new int[100];

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int iVol = sc.nextInt();
        getSlag(iVol, iVol, 0);
    }

    static void getSlag(int n, int k, int i){
        if(n < 0 || k < 0) return;
        if (n == 0){
            for (int j = 0; j < i; j++)
                System.out.println(massive[j] + " ");
            System.out.println();
        }
        else{
            if(n >= k){
                massive[i] = k;
                getSlag(n - k, k - 1, i + 1);
            }
            if (k > 1) getSlag(n, k-1, i);
        }
    }
}
