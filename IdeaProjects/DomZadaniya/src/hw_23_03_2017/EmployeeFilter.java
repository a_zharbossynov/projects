package hw_23_03_2017;

import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 * Created by MacBookAir on 26.03.17.
 */
public class EmployeeFilter {
    public static void main(String[] args){
        List<Employee> employeeList = Arrays.asList(
                new Employee("Dexter", true, true, true),
                new Employee("Yuki", false, true, true),
                new Employee("Kevin", false, false, true),
                new Employee("Miranda", true, true, false),
                new Employee("Kim", true, false, true));
        String isHappy = "isHappy";
        String isRich = "isRich";
        String isCompitent = "isCompitent";

        List<Employee> happyAndRich = employeeList.stream().filter(getSwitch(isHappy).
                and(getSwitch(isRich))).collect(Collectors.toList());
        List<Employee> compitentAndHappy = employeeList.stream().filter(getSwitch(isHappy).
                and(getSwitch(isCompitent))).collect(Collectors.toList());
        List<Employee> unhappyAndCompitent = employeeList.stream().filter(getSwitch(isHappy).
                negate().and(getSwitch(isCompitent))).collect(Collectors.toList());

        System.out.println(happyAndRich.stream().map(n -> n.getName()).
                reduce((new String("Happy and rich: ")),(acc,n) -> acc + " " + n));
        System.out.println(compitentAndHappy.stream().map(n -> n.getName()).
                reduce((new String("Happy and compitent: ")),(acc,n) -> acc + " " + n));
        System.out.println(unhappyAndCompitent.stream().map(n -> n.getName()).
                reduce((new String("Unhappy and compitent: ")),(acc,n) -> acc + " " + n));
    }

    public static Predicate<Employee> getSwitch(String emp){
        switch (emp){
            case "isHappy":
                return  (n -> n.isHappy());
            case "isRich":
                return  (n -> n.isRich());
            case "isCompitent":
                return  (n -> n.isCompitent());
            }
        return null;
    }
}


