package ru.itis.kpfu.Servlet;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import ru.itis.kpfu.Model.Group;
import ru.itis.kpfu.Model.Lab;
import ru.itis.kpfu.Model.User;
import ru.itis.kpfu.Service.GroupService;
import ru.itis.kpfu.Service.ScheduleService;
import ru.itis.kpfu.Service.Templater;
import ru.itis.kpfu.Service.UserService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "ProfileServlet", urlPatterns = {"/profile/*", "/logout/*"})
public class ProfileServlet extends HttpServlet{

    private static final String PATH_LOGOUT = "/logout";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        if(ProfileServlet.PATH_LOGOUT.equals(req.getServletPath())){
            session.invalidate();
            resp.sendRedirect("/");
            return;
        }

        if(!LoginServlet.checkSessionLogged(session)){
            resp.sendRedirect(LoginServlet.LINK_ERROR_ACCESS);
        }else{
            User user = (User) session.getAttribute("user");
            displayContent(req, resp, user);
        }
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String request = req.getParameter("request");
        if("change_password".equalsIgnoreCase(request)){
            this.changePassword(req, resp);
        }
        else if("change_fullname".equalsIgnoreCase(request)){
            this.changeFullname(req, resp);
        }
        else if("change_email".equalsIgnoreCase(request)){
            this.changeEmail(req, resp);
        }
        else if("change_group".equalsIgnoreCase(request)){
            this.changeGroup(req, resp);
        }
        else if("change_labs".equalsIgnoreCase(request)){
            this.changeLabs(req, resp);
        }
        else{
            resp.sendRedirect("/profile");
            System.out.println("Could not handle 'post' request: " + request);
        }

        int some = (request.equals("some")) ? 1 : 2;
    }

    private void displayContent(HttpServletRequest req, HttpServletResponse resp, User user) throws IOException{
        this.displayContent(req, resp, user, 0);
    }
    private void displayContent(HttpServletRequest req, HttpServletResponse resp, User user, int status) throws IOException {
        ServletContext context = req.getServletContext();
        Template templ = Templater.getTemplate(context, "profile.ftl");

        resp.setCharacterEncoding("UTF-8");
        PrintWriter writer = resp.getWriter();
        Map<String, Object> data = new HashMap<>();
        data.putAll(user.getHashMap());

        GroupService service = new GroupService();
        ArrayList<Group> groups = service.getAll();
        String notice = service.getNotice(user.getId_group());
        boolean editor = service.getEditorId(user.getId_group()) == user.getId();

        ScheduleService schedule = new ScheduleService();
        ArrayList<Integer> assignments = schedule.getAssignments(user.getId());
        ArrayList<Lab> labs = schedule.getLabs();

        for(Lab lab : labs){
            lab.setAssigned(assignments.contains(lab.getId()));
        }

        data.put("groups", groups);
        data.put("notice", notice);
        data.put("is_editor", editor);
        data.put("status", status);
        data.put("labs", labs);

        try {
            templ.process(data, writer);
        } catch (TemplateException e) {
            System.out.println("No templates were found(processed)");
            e.printStackTrace();
        }

        writer.flush();
        writer.close();
    }

    private void changePassword(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String pass1 = req.getParameter("pass1");
        String pass2 = req.getParameter("pass2");

        ensureVariables(req, (exist, user, service) -> {
            if(exist){
                boolean success = false;
                if(pass1.equals(pass2)) {
                    if (service.updatePassword(pass1, user.getId())) {
                        success = true;
                    }
                }

                displayContent(req, resp, user, (success)? 1 : -1);
            }else{
                handleNoVariables(req, resp);
            }
        });
    }
    private void changeFullname(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String fullname = req.getParameter("fullname");

        ensureVariables(req, (exist, user, service) -> {
            if(exist){
                boolean success = false;
                if(service.updateFullname(fullname, user.getId())){
                    success = true;
                    user.setFullname(fullname);
                    req.getSession().setAttribute("user", user);
                }

                displayContent(req, resp, user, (success)? 1 : -1);
            }else{
                handleNoVariables(req, resp);
            }
        });
    }
    private void changeEmail(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String email = req.getParameter("email");

        ensureVariables(req, (exist, user, service) -> {
            if(exist){
                boolean success = false;
                if(service.updateEmail(email, user.getId())){
                    success = true;
                    user.setEmail(email);
                    req.getSession().setAttribute("email", user);
                }

                displayContent(req, resp, user, (success)? 1 : -1);
            }else{
                handleNoVariables(req, resp);
            }
        });
    }
    private void changeGroup(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String s_id_group = req.getParameter("id_group");
        int id_group = Integer.parseInt(s_id_group);

        ensureVariables(req, (exist, user, service) -> {
            if(exist){
                boolean success = false;
                if(service.updateGroup(id_group, user.getId())){
                    success = true;
                    user.setId_group(id_group);
                    req.getSession().setAttribute("id_group", user);
                }

                displayContent(req, resp, user, (success)? 1 : -1);
            }else{
                handleNoVariables(req, resp);
            }
        });
    }
    private void changeLabs(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String s_id_lab = req.getParameter("id_lab");
        int id_lab = Integer.parseInt(s_id_lab);

        ensureVariables(req, (exist, user, service) -> {
            if(exist){
                boolean success = false;
                if(service.updateLab(id_lab, user.getId())){
                    success = true;
                }

                displayContent(req, resp, user, (success)? 1 : -1);
            }else{
                handleNoVariables(req, resp);
            }
        });
    }

    private void handleNoVariables(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        req.getSession().invalidate();
        resp.sendRedirect(LoginServlet.LINK_ERROR_ACCESS);
    }
    public static void ensureVariables(HttpServletRequest req, EnsuredVariablesCallback callback) throws IOException {
        HttpSession session = req.getSession();
        User user = (User) session.getAttribute("user");
        UserService service = (UserService) session.getAttribute("user_service");

        if(user != null && service != null){
            callback.ensured(true, user, service);
        }else{
            callback.ensured(false, null, null);
        }
    }

}

interface EnsuredVariablesCallback{

    void ensured(boolean exist, User user, UserService service) throws IOException;

}
