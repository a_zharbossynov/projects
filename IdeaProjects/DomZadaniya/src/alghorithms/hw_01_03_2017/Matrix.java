package alghorithms.hw_01_03_2017;

import java.util.Arrays;
import java.math.*;

/**
 * Created by MacBookAir on 09.03.17.
 */

public class Matrix {
    private  double[][] array ;


    public Matrix(double[][] array){
        try {
            if (array[1].length == array.length) {
            } else {
                throw new Exception();
            }
        }
        catch (Exception exception){
            System.out.println("Matrix is not square!");
        }
    }



    public static double getDeterminant(double[][] array){
        try {
            int sum = 0, k;
            if (array.length == 1) {
                return sum + array[0][0];
            }
            if (array[1].length == array.length) {
                double[][] array2;
                for (int j = 0; j < array.length; j++) {
                    if ((2 + j) % 2 != 0) {
                        k = -1;
                    } else {
                        k = 1;
                    }
                    array2 = getMatrixForDet(array, 0, j);
                    sum += k * array[0][j] * getDeterminant(array2);
                }
                return sum;
            } else {
                throw new Exception();
            }
        } catch (Exception exception) {
            System.out.println("Matrix is not square");
        }
        return 0;
    }



    public static double[][] getMatrixForDet(double[][] array, int i,int j) {
        double [][] array3 = new double[array.length - 1][array.length - 1];
        int z = 0, v = 0;
        for(int l = 0; l < array.length; l++){
            if(l != i) {
                for (int k = 0; k < array.length; k++) {
                    if (k != j) {
                        array3[v][z] = array[l][k];
                        z++;
                    }
                }
                z = 0;
                v++;
            }

        }
        return array3;
    }




    public static double[][] getMatDop(double[][] array){
        double [][] matDop = new double[array.length][array.length];
        for(int i = 0; i < array.length; i++){
            for(int j = 0; j <  array.length; j++){
                matDop[i][j] = getDop(array, i, j);
                System.out.println("А" + i + "" + j + " = " + matDop[i][j]);
            }
        }
        System.out.println("Matrix of algebraic complements" + "\n" + Arrays.deepToString(matDop));
        return matDop;
    }



    public static double getDop(double[][] array, int i, int j){
        int k = -1;
        if ((i + j) % 2 == 0){
            k = 1;
        }
        double [][] algDop = getMatrixForDet(array, i, j);
        return k * getDeterminant(algDop);
    }

    public static double[][] transpose(double[][] array) {
        double t = 0;
        double k = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (j >= k) {
                    t = array[i][j];
                    array[i][j] = array[j][i];
                    array[j][i] = t;
                }
            }
            k += 1;
        }
        System.out.println("Transpose matrix:" + "\n" + Arrays.deepToString(array));
        return array;
    }

    public static double[][] inverseMatrix(double[][] array){
        try {
            double det = getDeterminant(array);
            if ( det != 0) {
                double[][] invMat = multNumber(1/det,transpose(getMatDop(array)));
                return invMat;
            }
            else{
                throw new Exception();
            }
        }catch (Exception exception){
            System.out.println("Singular matrix!");
        }
        return null;
    }


    public static  double sk(double t){
        double k = t;
        if(Double.toString(t).length() > 7){
            k = new BigDecimal(t).setScale(3, RoundingMode.UP).doubleValue();
        }
        return k;
    }



    private static double[][] multNumber(double iDet, double[][] transpose) {
        String s;
        double t;
        for(int i = 0; i < transpose.length; i++){
            for(int j = 0; j < transpose.length; j++){
                transpose[i][j] = sk(transpose[i][j] * iDet);
            }
        }
        return transpose;
    }



    public static void solveSystem(double[][] array, double[] b){
        double[] x = new double[array.length];
        double[][] invMat = inverseMatrix(array);
        for(int i = 0; i < array.length; i++){
            for(int j = 0; j < array.length; j++){
                x[i] += invMat[i][j] * b[j];
            }
        }
        System.out.println("Solution of the system: ");
        for(int i = 0; i < x.length; i++){
            System.out.println("x(" + (i+1) + ") = " + sk(x[i]));
        }
    }


}
