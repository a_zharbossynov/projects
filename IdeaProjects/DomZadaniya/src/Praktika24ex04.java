/**
 * Практика-24. Найти в множестве все простые числа. Решить задачу 3 используя коллекции java.
 */
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
public class Praktika24ex04 {
    public static void main(String[] args) {
        Set set = new HashSet();
        set.add(3);
        set.add(4);
        set.add(5);
        set.add(7);
        set.add(8);
        set.add(9);
        Iterator iterator = set.iterator();
        while (iterator.hasNext()){
            boolean tempb = true;
            int temp = (Integer) iterator.next();
            for (int i = 2; i <= temp/2 ; i++ ){
                if (temp % i == 0) {
                    tempb = false;
                    break;
                }
            }
            if (tempb) System.out.println(temp);
        }
    }

}