public class Task055 {
    public static void main(String[] args) {
    }
}

class RationalMatrix2x2 extends RationalFraction{
    RationalFraction[][] arr = new RationalFraction[2][2];

    RationalMatrix2x2(){
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                arr[i][j] = new RationalFraction();
            }
        }
    }
    RationalMatrix2x2(RationalFraction num){
        for (int i = 0; i < 2; i++){
            for (int j = 0; j < 2; j++){
                arr[i][j] = num;
            }
        }
    }

    RationalMatrix2x2(RationalFraction m1, RationalFraction m2, RationalFraction m3, RationalFraction m4){
        arr[0][0] = m1;
        arr[0][1] = m2;
        arr[1][0] = m3;
        arr[1][1] = m4;
    }

    public RationalFraction getArr(int i, int j) {
        return arr[i][j];
    }

    RationalMatrix2x2 add(RationalMatrix2x2 Msv){
        RationalFraction[][] arr1 = new RationalFraction[2][2];
        for (int k = 0; k < 2; k++) {
            for (int i = 0; i < 2; i++) {
                arr1[k][i] = (arr[k][i]).add(Msv.getArr(k, i));
            }
        }
        return new RationalMatrix2x2(arr1[0][0], arr1[0][1], arr1[1][0], arr1[1][1]);
    }

    RationalMatrix2x2 mult(RationalMatrix2x2 Msv){
        RationalFraction[][] arr1 = new RationalFraction[2][2];
        for (int k = 0; k < 2; k++) {
            for (int i = 0; i < 2; i++) {
                arr1[k][i] = (arr[k][i]).mult(Msv.getArr(k, i));
            }
        }
        return new RationalMatrix2x2(arr1[0][0], arr1[0][1], arr1[1][0], arr1[1][1]);
    }
    RationalFraction det(){
        RationalFraction determinant = (arr[0][0].add(arr[1][1])).sub(arr[0][1].add(arr[1][0]));
        return determinant;
    }
}
