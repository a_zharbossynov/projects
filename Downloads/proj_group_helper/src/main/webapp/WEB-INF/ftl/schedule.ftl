<#-- @ftlvariable name="is_admin" type="java.lang.Boolean" -->
<#-- @ftlvariable name="is_editor" type="java.lang.Boolean" -->
<#-- @ftlvariable name="schedule" type="ru.itis.kpfu.Model.Schedule" -->
<#-- @ftlvariable name="fullname" type="java.lang.String" -->
<#-- @ftlvariable name="notice" type="java.lang.String" -->

<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Расписание</title>

    <!-- Bootstrap core CSS + icons -->
    <link href="/resources/css/bootstrap.css" rel="stylesheet">
    <link href="/resources/css/offcanvas.css" rel="stylesheet">
    <link href="/resources/fonts/font-awesome.min.css" rel="stylesheet">

    <#--JavaScript-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>

</head>

<body style="background: #f2f2f2">

<nav class="navbar navbar-expand-md fixed-top navbar-dark" style="background-color: #0c2954;">
    <a class="navbar-brand" href="#">Group helper</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler"
            aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarToggler">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <div class="d-md-none d-lg-none d-xl-none">
                <li class="nav-item active">
                    <a class="nav-link" href="/schedule/">Расписание <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/profile/">Профиль</a>
                </li>
            </div>
        </ul>
        <a href="/logout/" class="btn btn-outline-light" role="button">Выйти</a>
    </div>
</nav>

<div class="container" style="margin-bottom: 20px">
    <div class="row row-offcanvas row-offcanvas-right">

        <#--Main section-->
        <div class="col-12 col-md-9">

            <#--Notice-->
            <#if notice?? || is_editor>
                <div class="alert alert-light alert-dismissible fade show" role="alert">
                    <h4 class="alert-heading">
                        Объявление
                        <#if is_editor>
                            <a href="/notice/"><i class="fa fa-edit" style="color: coral; margin-left: 10px"></i></a>
                        </#if>
                    </h4>
                    <#if notice??>
                        ${notice}
                    </#if>
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </#if>

            <#--Schedule itself-->
            <#if schedule??>
                <div class="card">
                    <div class="card-body">
                        <nav class="nav nav-tabs" id="myTab" role="tablist">
                            <#list schedule.getSchedule() as day>
                                <a class="nav-item nav-link<#if day.isCurrent()> active</#if>"
                                   id="nav_tab_${day.getID()}" href="#${day.getID()}" aria-controls="${day.getID()}"
                                   aria-selected="<#if day.isCurrent()>true<#else>false</#if>"
                                   data-toggle="tab" role="tab">
                                    ${day.getName()}
                                </a>
                            </#list>
                        </nav>

                        <div class="tab-content">
                        <#list schedule.getSchedule() as day>
                            <div class="tab-pane fade<#if day.isCurrent()> show active</#if>" id="${day.getID()}" role="tabpanel"
                                 aria-labelledby="nav_tab_${day.getID()}">
                                <table class="table table-striped">
                                    <thead>
                                        <tr>
                                            <th>Время</th>
                                            <th>Предмет</th>
                                            <th>ДЗ</th>
                                            <th>Аудитроия</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <#list day.getLessons() as lesson>
                                            <tr>
                                                <th scope="row">${lesson.getTimeframe()}</th>
                                                <td>${lesson.getName()}</td>
                                                <td>${lesson.getHometask()}</td>
                                                <td>${lesson.getAuditorium()}</td>
                                            </tr>
                                        </#list>
                                    </tbody>
                                </table>
                            </div>
                        </#list>
                        </div>

                        <#--Editor's buttons-->
                        <#if is_editor?? && is_editor>
                            <div id="edit_buttons" style="margin-top: 5px;">
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#edit_modal">
                                    Редактировать
                                </button>
                                <button type="button" class="btn btn-primary" data-toggle="modal" data-target="#add_hw_modal">
                                    Добавить ДЗ
                                </button>
                            </div>
                        </#if>
                    </div>
                </div>
            <#else>
                <h2>
                    Error retrieving data :c
                </h2>
            </#if>
        </div>

        <#--Navigation-->
        <div class="col-6 col-md-3 sidebar-offcanvas" id="sidebar">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Привет, ${fullname}!</h4>
                </div>
                <div class="list-group">
                    <a href="/schedule/" class="list-group-item active">Расписание</a>
                    <a href="/profile/" class="list-group-item">Профиль</a>
                </div>
            </div>
            <br>
        </div>
    </div>
</div>

<!-- Edit Timetable Modal -->
<div class="modal fade" id="edit_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Редактирование расписания</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="/schedule/" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <select class="form-control" name="weekday" id="edit_day" required>
                            <option disabled selected>День недели</option>
                            <option value="1">Понедельник</option>
                            <option value="2">Вторник</option>
                            <option value="3">Среда</option>
                            <option value="4">Четверг</option>
                            <option value="5">Пятница</option>
                            <option value="6">Суббота</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="ordinal" id="edit_time" required>
                            <option disabled selected>Время пары</option>
                            <option value="0">8:30-10:00</option>
                            <option value="1">10:10-11:40</option>
                            <option value="2">11:50-13:20</option>
                            <option value="3">13:40-15:10</option>
                            <option value="4">15:20-16:50</option>
                            <option value="5">17:00-18:30</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="title" id="edit_subject" aria-describedby="emailHelp"
                               placeholder="Наименование предмета" required>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="auditorium" id="edit_classroom"
                               aria-describedby="emailHelp" placeholder="Аудитория" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отменить</button>
                    <input type="submit" class="btn btn-primary" value="Применить"/>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Edit Hometask Modal -->
<div class="modal fade" id="add_hw_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Добавить ДЗ</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="/schedule/" method="post">
                <div class="modal-body">
                    <div class="form-group">
                        <select class="form-control" name="weekday" id="add_hw_day" required>
                            <#list 1..6 as i>
                                <#assign day = schedule.getSchedule()[i - 1]>
                                <option value="${i}"<#if day.isCurrent()> selected</#if>>${day.getName()}</option>
                            </#list>
                        </select>
                    </div>
                    <div class="form-group">
                        <select class="form-control" name="ordinal" id="add_hw_time" required>
                            <option disabled selected>Время пары</option>
                            <option value="0">8:30-10:00</option>
                            <option value="1">10:10-11:40</option>
                            <option value="2">11:50-13:20</option>
                            <option value="3">13:40-15:10</option>
                            <option value="4">15:20-16:50</option>
                            <option value="5">17:00-18:30</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="hometask" id="hw" aria-describedby="emailHelp"
                               placeholder="Домашнее задание" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Отмена</button>
                    <input type="submit" class="btn btn-primary" value="Применить"/>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script src="http://getbootstrap.com/assets/js/vendor/popper.min.js"></script>
<script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
</body>
</html>
