public class Task052 {
    public static void main(String[] args) {
    }
}

class Matrix2x2 {
    double[][] arr = new double[2][2];

    Matrix2x2() {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                arr[i][j] = 0;
            }
        }
    }

    Matrix2x2(double a) {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                arr[i][j] = a;
            }
        }
    }

    Matrix2x2(double[][] arr) {
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                this.arr[i][j] = arr[i][j];
            }
        }
    }

    Matrix2x2(double m1, double m2, double m3, double m4) {
        arr[0][0] = m1;
        arr[0][1] = m2;
        arr[1][0] = m3;
        arr[1][1] = m4;
    }

    public double getArr(int i, int j) {
        return arr[i][j];
    }

    Matrix2x2 add(Matrix2x2 Msv) {
        double[][] arr1 = new double[2][2];
        for (int k = 0; k < 2; k++) {
            for (int i = 0; i < 2; i++) {
                arr1[k][i] = arr[k][i] + Msv.getArr(k, i);
            }
        }
        return new Matrix2x2(arr1);
    }

    void add2(Matrix2x2 Msv) {
        for (int k = 0; k < 2; k++) {
            for (int i = 0; i < 2; i++) {
                arr[k][i] = arr[k][i] + Msv.getArr(k, i);
            }
        }
    }
    Matrix2x2 sub(Matrix2x2 Msv) {
        double[][] arr1 = new double[2][2];
        for (int k = 0; k < 2; k++) {
            for (int i = 0; i < 2; i++) {
                arr1[k][i] = arr[k][i] - Msv.getArr(k, i);
            }
        }
        return new Matrix2x2(arr1);
    }
    void sub2(Matrix2x2 Msv) {
        for (int k = 0; k < 2; k++) {
            for (int i = 0; i < 2; i++) {
                arr[k][i] = arr[k][i] - Msv.getArr(k, i);
            }
        }
    }
    Matrix2x2 multNumber(double number) {
        double[][] arr1 = new double[2][2];
        for (int k = 0; k < 2; k++) {
            for (int i = 0; i < 2; i++) {
                arr1[k][i] = arr[k][i]*number;
            }
        }
        return new Matrix2x2(arr1);
    }
    void multNumber2(double number) {
        for (int k = 0; k < 2; k++) {
            for (int i = 0; i < 2; i++) {
                arr[k][i] = arr[k][i] * number;
            }
        }
    }
    Matrix2x2 mult(Matrix2x2 Msv) {
        double[][] arr1 = new double[2][2];
        for (int k = 0; k < 2; k++) {
            for (int i = 0; i < 2; i++) {
                arr1[k][i] = arr[k][i] * Msv.getArr(k, i);
            }
        }
        return new Matrix2x2(arr1);
    }
    void mult2(Matrix2x2 Msv) {
        for (int k = 0; k < 2; k++) {
            for (int i = 0; i < 2; i++) {
                arr[k][i] = arr[k][i] * Msv.getArr(k, i);
            }
        }
    }
    double det(){
        double determinant = arr[0][0] * arr[1][1] - arr[1][0] * arr[0][1];
        return determinant;
    }
    void transpon(){
        double k = 0;
        arr[0][1] = k;
        arr[0][1] = arr[1][0];
        arr[1][0] = k;
    }
    Matrix2x2 inverseMatrix(Matrix2x2 Msv) {
        double[][] arr1 = new double[2][2];
        double det = arr[0][0] * arr[1][1] - arr[1][0] * arr[0][1];
        if (det == 0){
            System.out.println("we can't inverse it");
        }else{
            arr1[0][0] = arr[1][1] / det;
            arr1[0][1] = (-1) * arr[1][0] / det;
            arr1[1][0] = (-1) * arr[0][1] / det;
            arr1[1][1] = arr[0][0] / det;
        }
        return new Matrix2x2(arr1);
    }


}
