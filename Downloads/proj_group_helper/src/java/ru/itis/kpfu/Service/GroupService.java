package ru.itis.kpfu.Service;

import ru.itis.kpfu.DAO.GroupDAO;
import ru.itis.kpfu.Model.Group;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class GroupService implements GroupDAO {


    //MARK: - Retrieving data
    @Override
    public ArrayList<Group> getAll(){
        String raw = "SELECT * FROM groups ORDER BY title";
        ArrayList<Group> groups = new ArrayList<>();

        try {
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);
            ResultSet set = statement.executeQuery();

            while(set.next()){
                groups.add(new Group(set));
            }

            connection.close();
            statement.close();
            set.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return groups;
    }
    @Override
    public Group getById(int id) {
        String raw = "SELECT * FROM groups WHERE id=? LIMIT 1";
        Group group = null;

        try {
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);
            statement.setInt(1, id);
            ResultSet set = statement.executeQuery();

            if(set.next()){
                group = new Group(set);
            }

            connection.close();
            statement.close();
            set.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }


        return group;
    }
    @Override
    public String getNotice(int id_group) {
        String raw = "SELECT text FROM notices WHERE id_group=? LIMIT 1";
        String notice = null;

        try {
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);
            statement.setInt(1, id_group);
            ResultSet set = statement.executeQuery();

            if(set.next()){
                notice = set.getString("text");
            }

            connection.close();
            statement.close();
            set.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return notice;
    }
    @Override
    public int getEditorId(int id_group) {
        String raw = "SELECT id_editor FROM groups WHERE id=?";
        int id = -1;

        try{
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);
            statement.setInt(1, id_group);
            ResultSet set = statement.executeQuery();

            if(set.next()){
                id = set.getInt("id_editor");
            }

            connection.close();
            statement.close();
            set.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return id;
    }

    //MARK: - Setting / Updating data
    @Override
    public boolean setNotice(String notice, int id_group) {
        boolean existed = getNotice(id_group) != null;
        boolean done = false;
        String raw;

        if(!existed){
            raw = "INSERT INTO notices (id_group, text) VALUES (?, ?)";
        }else{
            raw = "UPDATE notices SET text=? WHERE id_group=?";
        }
        raw += " RETURNING text";

        try{
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);

            if(!existed){
                statement.setInt(1, id_group);
                statement.setString(2, notice);
            }else{
                statement.setString(1, notice);
                statement.setInt(2, id_group);
            }

            ResultSet set = statement.executeQuery();

            if(set.next()){
                done = set.getString(1) != null;
            }

            connection.close();
            statement.close();
            set.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return done;
    }
}
