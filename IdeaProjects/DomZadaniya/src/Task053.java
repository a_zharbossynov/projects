public class Task053 {
    public static void main(String[] args) {
    }
}

class RationalVector2D extends RationalFraction{
    RationalFraction x, y;
    RationalVector2D(){
        x.numerator = 0;
        y.numerator = 0;
    }

    RationalVector2D(RationalFraction x, RationalFraction y){
        this.x = x;
        this.y = y;
    }

    public RationalFraction getX() {
        return x;
    }

    public RationalFraction getY() {
        return y;
    }

    RationalVector2D add(RationalVector2D vector){
        RationalFraction vectorX = x.add(vector.getX());
        RationalFraction vectorY = y.add(vector.getY());
        return new RationalVector2D(vectorX, vectorY);
    }

    public String toString(){
        String s = "(" + x.toString() + ";" + y.toString() + ")";
        return s;
    }
    double length(){
        double vecLength = Math.sqrt(x.value()*x.value() + y.value()*y.value());
        return vecLength;
    }
    RationalFraction scalarProduct(RationalVector2D vector2D){
        RationalFraction scProduct = (x.mult(vector2D.getX())).add(y.mult(vector2D.getY()));
        return  scProduct;
    }
    boolean equals(RationalVector2D vector2D){
        boolean vector = x.equals(vector2D.getX()) && y.equals(vector2D.getY());
        return vector;
    }

}
