package hw_02_03_2017.Pattern_Decorator;
import java.util.Arrays;

public class SimpleDisplayString implements IArrayString {

    private String[] arr;
    public SimpleDisplayString(String[] arr){
        this.arr = arr;
    }
    public String toDisplay(){
        return Arrays.toString(arr);
    }
    public String[] getArr(){
        return arr;
    }

}
