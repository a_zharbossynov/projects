package hw_02_03_2017.Pattern_Template_Method;

/**
 * Created by MacBookAir on 06.03.17.
 */

public class GlassHouse extends HouseTemplate {

    @Override
    public void buildWalls() {
        System.out.println("Building Glass Walls");
    }

    @Override
    public void buildPillars() {
        System.out.println("Building Pillars with glass coating");
    }

}
