package cw_23_03_17;

/**
 * Created by MacBookAir on 23.03.17.
 *
 * Проверочная
 * Задание номер 2.
 */
public class Task2_1st {
    public static void main(String[] args) {
        String str1 = "Porsche";
        String str2 = "Maserati";

        WhichOneBest bestString = (obj1, obj2) -> (obj1.compareTo(obj2) < 0) ? obj1 : obj2;
        System.out.println(bestString.func(str1, str2));
    }
}

@FunctionalInterface
interface WhichOneBest{
    String func(String str1, String str2);
}
