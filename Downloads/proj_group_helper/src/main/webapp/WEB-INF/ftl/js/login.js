jQuery( "#form_login" ).submit(function( event ) {

    alert("jQuery is triggered, BTW")
    var data = $("#form_login").serialize();

    jQuery.post("/login", data, function(response) {
        var s_json = JSON.stringify(response);
        var json = jQuery.parseJSON(s_json);

        if(json.status === 0){
            var lbl_status = jQuery("#lbl_status");
            lbl_status.css("color", "red");
            lbl_status.text(json.msg_error);
        }
        else if(json.status === 1){
            window.location.assign("/profile");
        }
        else{
            alert("Hm... Weird...")
        }

    }, "json").fail( function(jqXHR, textStatus){
        alert(textStatus);
    });

    event.preventDefault();
});