package hw_02_03_2017.Pattern_Decorator;
import java.util.Arrays;

public class DisplayArraySort extends ArrayDisplayDecorator {

    private String [] arr;
    public DisplayArraySort(IArrayString basicDisplay){
        originalDisplay = basicDisplay;
    }
    public String[] getArr(){
        return arr;
    }
    @Override
    public String toDisplay(){
        return toDisplayWithSort(originalDisplay);
    }

    public String toDisplayWithSort(IArrayString display){
        this.arr = display.getArr().clone();
        Arrays.sort(this.arr);
        return  Arrays.toString(this.arr);
    }
}
