package ru.itis.kpfu.Service;

import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateExceptionHandler;

import javax.servlet.ServletContext;
import java.io.IOException;

public class Templater {

    private static Configuration config;


    //MARK: - Initializers
    private static void initFreemaker(){
        config = new Configuration(Configuration.VERSION_2_3_27);
        config.setTemplateExceptionHandler(TemplateExceptionHandler.HTML_DEBUG_HANDLER);
        config.setLogTemplateExceptions(false);
        config.setDefaultEncoding("UTF-8");
    }

    //MARK: - Convenience
    public static Template getTemplate(ServletContext context, String file){
        getConfig().setServletContextForTemplateLoading(context, "WEB-INF/ftl");

        try {
            return getConfig().getTemplate(file);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static Configuration getConfig(){
        if(config != null){
            return config;
        }else{
            initFreemaker();
            return config;
        }
    }
}
