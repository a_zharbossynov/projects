package hw_02_03_2017.Pattern_State;

/**
 * Created by MacBookAir on 06.03.17.
 */

public class TVStartState implements IState {

    @Override
    public void doAction() {
        System.out.println("TV is turned ON");
    }

}
