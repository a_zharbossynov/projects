<#-- @ftlvariable name="labs" type="com.sun.tools.javac.util.List<ru.itis.kpfu.Model.Lab>" -->
<#-- @ftlvariable name="status" type="java.lang.Integer" -->
<#-- @ftlvariable name="id_group" type="java.lang.Integer" -->
<#-- @ftlvariable name="is_editor" type="java.lang.Boolean" -->
<#-- @ftlvariable name="notice" type="java.lang.String" -->
<#-- @ftlvariable name="groups" type="com.sun.tools.javac.util.List<ru.itis.kpfu.Model.Group>" -->
<#-- @ftlvariable name="login" type="java.lang.String" -->
<#-- @ftlvariable name="email" type="java.lang.String" -->
<#-- @ftlvariable name="fullname" type="java.lang.String" -->
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">
    <title>Профиль пользователя</title>

    <!-- Bootstrap core CSS + icons -->
    <link href="/resources/css/bootstrap.css" rel="stylesheet">
    <link href="/resources/css/offcanvas.css" rel="stylesheet">
    <link href="/resources/fonts/font-awesome.min.css" rel="stylesheet">

    <#--JavaScript-->
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>

</head>

<body style="margin-bottom: 20px">

<nav class="navbar navbar-expand-md fixed-top navbar-dark" style="background-color: #0c2954;">
    <a class="navbar-brand" href="#">Group helper</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarToggler"
            aria-controls="navbarToggler" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarToggler">
        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
            <div class="d-md-none d-lg-none d-xl-none">
                <li class="nav-item">
                    <a class="nav-link" href="/schedule/">Расписание <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/profile/">Профиль</a>
                </li>
            </div>
        </ul>
        <a href="/logout/" class="btn btn-outline-light" role="button">Выйти</a>
    </div>
</nav>

<div class="container">

    <div class="row row-offcanvas row-offcanvas-right">
        <div class="col-12 col-md-9">
            <h1>Профиль</h1><br>
            <div class="row">
                <div class="col-11">
                    <#if status?? && status != 0>
                        <div class="alert ${(status == -1)?then("alert-danger", "alert-success")} alert-dismissible fade show" role="alert">
                            <h4 class="alert-heading">${(status == -1)?then("Ошибка!", "Успешно!")}</h4>
                                ${
                                    (status == -1)?then(
                                    "Введённые Вами данные не соответствуют критериям!",
                                    "Данные успешно изменены!")
                                }
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </#if>

                    <h3><u>Изменение пароля</u></h3>
                    <form id="form_password" action="/profile/?request=change_password" method="post">
                        <div class="form-group">
                            <input type="password" class="form-control" id="fld_pass_first" name="pass1" placeholder="Новый пароль" required>
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="fld_pass_secnd" name="pass2" placeholder="Новый пароль ещё раз" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </form>
                    <br>
                    <h3><u>Изменение псевдонима</u></h3>
                    <form id="form_username" action="/profile/?request=change_fullname" method="post">
                        <div class="form-group">
                            <input type="text" class="form-control" name="fullname" id="fld_username" value="${fullname}" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </form>
                    <br>
                    <h3><u>Изменение эл. почты</u></h3>
                    <form action="profile/?request=change_email" method="post">
                        <div class="form-group">
                            <input type="email" class="form-control" name="email" aria-describedby="emailHelp"
                                   value="${email}" required>
                        </div>
                        <button type="submit" class="btn btn-primary">Сохранить</button>
                    </form>
                    <#if groups??>
                        <br>
                        <h3><u>Изменение учебной группы</u></h3>
                        <form action = "profile/?request=change_group" method="post">
                            <div class="form-group">
                                <select class="form-control" id="electives" name="id_group" required>
                                    <#list groups as group>
                                        <option value="${group.getId()}"<#if group.getId() == id_group> selected</#if>>${group.getTitle()}</option>
                                    </#list>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </form>
                    </#if>
                    <#if labs??>
                        <br>
                        <h3><u>Изменение курсов по выбору</u></h3>
                        <form action="profile/?request=change_labs" method="post">
                            <div class="form-group">
                                <select class="form-control" name="id_lab" required>
                                    <option selected>Не выбрано</option>

                                    <#list labs as lab>
                                        <#assign assigned = lab.getIsAssigned()>
                                        <option value="${lab.getId()}"<#if assigned> selected</#if>>${lab.getTitle()}</option>
                                    </#list>
                                </select>
                            </div>
                            <button type="submit" class="btn btn-primary">Сохранить</button>
                        </form>
                    </#if>
                </div>
            </div>
        </div><!--/span-->

        <div class="col-6 col-md-3 sidebar-offcanvas" id="sidebar">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">Привет, ${fullname}!</h4>
                </div>
                <div class="list-group">
                    <a href="/schedule/" class="list-group-item">Расписание</a>
                    <a href="Profile.html" class="list-group-item active">Профиль</a>
                </div>
            </div>
            <br>

            <#if notice?? || is_editor>
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">
                        Объявление
                        <#if is_editor?? && is_editor == true>
                            <a href="/notice/"><span class="fa fa-edit" aria-hidden="true" style="color: coral; margin-left: 10px"></span></a>
                        </#if>
                    </h4>
                    <p class="card-text">
                        <#if notice??>
                            ${notice}
                        </#if>
                    </p>
                </div>
            </div>
            </#if>
        </div><!--/span-->
    </div><!--/row-->
</div><!--/.container-->

<#--JavaScript-->
<script src="../../resources/js/profile.js"></script>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="http://getbootstrap.com/assets/js/vendor/popper.min.js"></script>
<script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
<script src="http://getbootstrap.com/docs/4.0/examples/offcanvas/offcanvas.js"></script>
</body>
</html>
