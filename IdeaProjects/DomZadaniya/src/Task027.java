/**
 * @author Alisher Zharbossynov
 * 11-602
 * Task027
 */
import java.util.Scanner;
public class Task027 {
    public static void main(String[] args) {
        Scanner task027 = new Scanner(System.in);
        System.out.println("Vvedite kol-vo n: ");
        int n = task027.nextInt();
        int[] arr = new int[n];
        System.out.println("Vvedite peremennye n: ");
        for (int i = 0; i < n; i++){
            arr[i] = task027.nextInt();
        }
        for (int j = 0; j < n; j++){
            if (((arr[j] % 2 == 0) && (arr[j] % 3 == 0)) || (( arr[j] % 5 == 0) && (arr[j] % 6 == 0))) {
                System.out.println(arr[j]);
            }
        }
    }
}
