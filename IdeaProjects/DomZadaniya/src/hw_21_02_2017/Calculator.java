package hw_21_02_2017;

/**
 * Created by MacBookAir on 27.02.17.
 */

public class Calculator implements ICalculator{
    public Calculator(double a, String sym, double b) {
        if (sym.equals("+")) {
            System.out.println(a + " + " + b + " = " + add(a,b));
            return;
        }
        if (sym.equals("-")) {
            System.out.println(a + " - " + b + " = " + sub(a,b));
            return;
        }
        if (sym.equals("*")) {
            System.out.println(a + " * " + b + " = " + mult(a,b));
            return;
        }
        if (sym.equals("/")) {
            System.out.println(a + " / " + b + " = " + div(a,b));
            return;
        }
        System.out.println("Unknown operation");
    }

    public double add(double a, double b) {
        return a + b;
    }

    public double sub(double a, double b) {
        return a - b;
    }

    public double mult(double a, double b) {
        return a * b;
    }

    public double div(double a, double b) {
        return a / b;
    }
}
