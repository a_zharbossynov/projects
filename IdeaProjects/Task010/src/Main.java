import java.util.Scanner;

/**
 * @author Alisher Zharbossynov
 * 11-602
 * Task 010
 */

public class Main {

    public static void main(String[] args) {
        Scanner Main = new Scanner(System.in);
        double x, y;
        System.out.println("Введите переменную x: ");
        x = Main.nextDouble();
        y = x > 2? (x*x - 1)/(x+2) : x > 0?(x*x - 1)*(x+2):x*x*(1 + 2*x);
        System.out.println(y);
            }
        }