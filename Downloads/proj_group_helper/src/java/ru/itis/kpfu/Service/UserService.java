package ru.itis.kpfu.Service;

import ru.itis.kpfu.DAO.UserDAO;
import ru.itis.kpfu.Model.User;
import ru.itis.kpfu.Model.UserBlueprint;

import javax.xml.crypto.Data;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class UserService implements UserDAO {

    //MARK: - Retrieving
    public ArrayList<User> getAll() {
        String raw = "SELECT * FROM profiles";
        ArrayList<User> users = new ArrayList<User>();

        try {
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);
            ResultSet set = statement.executeQuery();

            while(set.next()){
                User user = new User(set);
                users.add(user);
            }

            connection.close();
            statement.close();
            set.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return users;
    }
    public ArrayList<User> getGroup(int id) {
        String raw = "SELECT * FROM profiles WHERE id_group=?";
        ArrayList<User> users = new ArrayList<User>();

        try {
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);
            statement.setInt(1, id);
            ResultSet set = statement.executeQuery();

            while(set.next()){
                User user = new User(set);
                users.add(user);
            }

            connection.close();
            statement.close();
            set.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return users;
    }
    public User getByLogin(String login) {
        String raw = "SELECT * FROM profiles WHERE login=? LIMIT 1";
        User user = null;

        try {
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);
            statement.setString(1, login.toLowerCase());
            ResultSet set = statement.executeQuery();

            if(set.next()){
                user = new User(set);
            }

            connection.close();
            statement.close();
            set.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }
    public User getByID(int id) {
        String raw = "SELECT * FROM profiles WHERE id=? LIMIT 1";
        User user = null;

        try {
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);
            statement.setInt(1, id);
            ResultSet set = statement.executeQuery();

            if(set.next()){
                user = new User(set);
            }

            connection.close();
            statement.close();
            set.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }

    //MARK: - Checking
    public User auth(String login, String pass) {
        String raw = "SELECT * FROM profiles WHERE login=? AND password=? LIMIT 1";
        User user = null;

        try {
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);
            statement.setString(1, login.toLowerCase());
            statement.setString(2, this.hashed(pass));
            ResultSet set = statement.executeQuery();

            if(set.next()){
                user = new User(set);
            }

            connection.close();
            statement.close();
            set.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return user;
    }
    public boolean checkLoginAvailable(String login){
        String raw = "SELECT login FROM profiles WHERE login=? LIMIT 1";
        boolean available = true;

        try {
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);
            statement.setString(1, login.toLowerCase());
            ResultSet set = statement.executeQuery();
            available = !set.next();

            set.close();
            connection.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return available;
    }
    public boolean checkEmailAvailable(String email){
        String raw = "SELECT email FROM profiles WHERE email=? LIMIT 1";
        boolean available = true;

        try {
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);
            statement.setString(1, email);
            ResultSet set = statement.executeQuery();
            available = !set.next();

            set.close();
            connection.close();
            statement.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return available;
    }

    //MARK: - Managing
    public int add(UserBlueprint user) {
        String raw = "INSERT INTO profiles (login, email, password, fullname, id_group) ";
        raw += "VALUES (?, ?, ?, ?, ?) ";
        raw += "RETURNING id";
        int id = -1;

        try {
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);
            statement.setString(1, user.getLogin().toLowerCase());
            statement.setString(2, user.getEmail());
            statement.setString(3, this.hashed(user.getPassword()));
            statement.setString(4, user.getFullname());
            statement.setInt(5, user.getId_group());
            ResultSet set = statement.executeQuery();

            if(set.next()){
                id = set.getInt("id");
            }

            connection.close();
            statement.close();
            set.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return id;
    }
    public boolean remove(int id) {
        return false;
    }

    //MARK: - Updating
    public boolean updatePassword(String pass, int id) {
        return update("password", hashed(pass), id);
    }
    public boolean updateFullname(String fullname, int id) {
        return update("fullname", fullname, id);
    }
    public boolean updateEmail(String email, int id) {
        return update("email", email, id);
    }
    public boolean updateGroup(int id_group, int id) {
        return update("id_group", id_group, id);
    }
    public boolean updateLab(int id_lab, boolean joined, int id) {
        return false;
    }
    public boolean updateLab(int id_lab, int id){
        String raw = "INSERT INTO lab_assignments VALUES (?, ?) ON CONFLICT (id_student) DO UPDATE SET id_lab = EXCLUDED.id_lab RETURNING id_student";
        boolean success = false;

        try{
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);
            statement.setInt(1, id);
            statement.setInt(2, id_lab);
            ResultSet set = statement.executeQuery();
            success = set.next();

            set.close();
            statement.close();
            connection.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return success;
    }
    private boolean update(String field, Object value, int id){
        String raw = "UPDATE profiles SET "+ field + "=? WHERE id=? RETURNING id";
        boolean success = false;

        try{
            Connection connection = Database.getConnection();
            PreparedStatement statement = connection.prepareStatement(raw);

            if(value instanceof Integer){
                statement.setInt(1, (int) value);
            }
            else if (value instanceof String){
                statement.setString(1, (String) value);
            }

            statement.setInt(2, id);
            ResultSet set = statement.executeQuery();
            success = set.next();

            connection.close();
            statement.close();
            set.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return success;
    }

    //MARK: – Cipher
    public String hashed(String password){
        String outcome = null;
        try {
            MessageDigest md5 = MessageDigest.getInstance("MD5");
            md5.update(password.getBytes());
            byte[] bytes = md5.digest();

            StringBuffer buffer = new StringBuffer();
            StringBuilder str = new StringBuilder();
            for(byte b : bytes){
                String temp = Integer.toHexString(b & 0xff).toString();
                if(temp.length() == 1){
                    temp = "0" + temp;
                }

                str.append(temp);
            }

            outcome = str.toString();
        } catch (NoSuchAlgorithmException e) {
            System.out.println("•••Failed to hash password");
            e.printStackTrace();
        }

        return outcome;
    }
}
