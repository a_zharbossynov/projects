package ru.itis.kpfu.Service;

import org.apache.commons.dbcp2.BasicDataSource;

import java.sql.*;

public class Database {

    private static BasicDataSource source;
    private static Connection connection;

    //MARK: - Initializers
    private static boolean initConnection(){
        try {
            Class.forName("org.postgresql.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }

        System.out.println("PostgreSQL JDBC Driver Registered!");
        connection = null;

        try {
            connection = DriverManager.getConnection(
                    "jdbc:postgresql://localhost:5432/group_helper", "postgres",
                    "p4ssw0rd");

        } catch (SQLException e) {
            System.out.println("Database Connection Failed!");
            e.printStackTrace();
            return false;
        }

        if (getConnection() != null) {
            System.out.println("Database successfully connected!");
            return true;
        } else {
            System.out.println("Failed to make database connection!");
            return false;
        }
    }
    private static void initSource(){
        source = new BasicDataSource();
        source.setDriverClassName("org.postgresql.Driver");
        source.setUsername("postgres");
        source.setPassword("p4ssw0rd");
        source.setUrl("jdbc:postgresql://localhost:5432/group_helper");
    }

    //MARK: - Convenience
    public static Connection getConnection(){
        if(source == null){
            initSource();
        }

        try {
            return source.getConnection();
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }
//    public static Connection getConnection(){
//        if(connection != null){
//            return connection;
//        }else{
//            if (initConnection()){
//                return connection;
//            }else{
//                return null;
//            }
//        }
//    }


}
