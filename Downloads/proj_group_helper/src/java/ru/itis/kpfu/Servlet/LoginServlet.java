package ru.itis.kpfu.Servlet;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.json.JSONObject;
import ru.itis.kpfu.Model.User;
import ru.itis.kpfu.Service.Templater;
import ru.itis.kpfu.Service.UserService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Writer;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "LoginServlet", urlPatterns = {"", "/login/*"})
public class LoginServlet extends HttpServlet {

    public static final String LINK_ERROR_ACCESS = "/?error=" + LoginServletError.ACCESS_DENIED.code();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        Cookie[] cookies = req.getCookies();

        boolean logged = LoginServlet.checkSessionLogged(session);
//        logged = logged || this.checkCookiesLogged(cookies);

        if(logged){
            this.redirectToSchedule(resp);
        }else{
            LoginServletError error = LoginServletError.valueFor(req.getParameter("error"));
            this.displayForm(req, resp, error, null);
        }
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setStatus(HttpServletResponse.SC_OK);
        resp.setCharacterEncoding("UTF-8");

        HttpSession session = req.getSession();
        if(checkSessionLogged(session)){
            System.out.println("session logged");
            this.redirectToSchedule(resp);
            return;
        }

        String login = req.getParameter("login");
        String pass = req.getParameter("pass");

        if(tryLogin(login, pass, session)){
            this.setLogged(session);
//            this.respondLogged(true, resp);
            this.redirectToSchedule(resp);
        }
        else{
            this.displayForm(req, resp, LoginServletError.NO_MATCH, login);
//            this.respondLogged(false, resp);
        }
    }

    //MARK: - Functionality
    private void setLogged(HttpSession session){
        if(session == null){
            return;
        }

        session.setAttribute("logged", true);
    }

    //MARK: - Responding
    private void respondLogged(boolean success, HttpServletResponse resp) throws IOException {
        resp.setContentType("application/json;charset=UTF-8");
        Writer out = resp.getWriter();
        String s_resp;

        if (success){
            s_resp = this.getSuccessString();
        }else{
            s_resp = this.getErrorString();
        }

        out.write(s_resp);
        out.flush();
        out.close();
    }

    //MARK: - Checking
    public static boolean tryLogin(String login, String pass, HttpSession session){
        UserService userService = (UserService) session.getAttribute("user_service");
        if(userService == null) {
            userService = new UserService();
            session.setAttribute("user_service", userService);
        }

        User user = userService.auth(login, pass);
        session.setAttribute("user", user);

        return user != null;
    }
    public static boolean checkSessionLogged(HttpSession session){
        Object raw = session.getAttribute("logged");
        Boolean logged = false;
        if(raw != null){
            logged = (Boolean) raw;
        }

        return logged;
    }
//    private boolean checkCookiesLogged(Cookie[] cookies){
//        String login = null, pass = null;
//
//        for (Cookie cookie : cookies) {
//            if (cookie.getName().equals("login")) {
//                login = cookie.getValue();
//            } else if (cookie.getName().equals("pass")) {
//                pass = cookie.getValue();
//            }
//        }
//
//
//        if(login != null && pass != null){
//            return this.tryLogin(login, pass);
//        }
//        else{
//            return false;
//        }
//    }

    //MARK: - Responding
    private void displayForm(HttpServletRequest req, HttpServletResponse resp, LoginServletError error, String login) throws IOException {
        ServletContext context = req.getServletContext();
        Template templ = Templater.getTemplate(context, "login.ftl");

        resp.setCharacterEncoding("UTF-8");
        PrintWriter writer = resp.getWriter();

        Map<String, Object> data = new HashMap<>();
        data.putAll(error.getData());

        if(login != null){
            data.put("login", login);
        }

        try {
            templ.process(data, writer);
        } catch (TemplateException e) {
            System.out.println("No templates were found(processed)");
            e.printStackTrace();
        }

        writer.flush();
        writer.close();
    }
    private void redirectToSchedule(HttpServletResponse resp) throws ServletException, IOException {
        resp.sendRedirect("/schedule");
    }

    //MARK: - Responses
    private String getSuccessString(){
        JSONObject json = new JSONObject();
        json.put("status", 1);
        return json.toString();
    }
    private String getErrorString(){
        JSONObject json = new JSONObject();
        json.put("status", 0);
        json.put("msg_error", "Credentials don't match!");
        return json.toString();
    }
}

enum LoginServletError{

    NONE (-1, ""),
    NO_MATCH (0, "Неверно введены логин и/или пароль!"),
    ACCESS_DENIED (1, "Для получения доступа к запрашиваемому ресурсу, необходимо авторизоваться!");

    private int status_code;
    private String message;

    LoginServletError(int status_code, String message) {
        this.status_code = status_code;
        this.message = message;
    }

    public String code(){
        return String.valueOf(this.status_code);
    }
    public Map<String, Object> getData(){
        Map<String, Object> data = new HashMap<>();

        switch(this){
            case NONE:
                break;

            default:
                data.put("msg_error", this.message);
                break;
        }

        return data;
    }
    public static  LoginServletError valueFor(String status_code){
        if(status_code == null){
            return NONE;
        }else{
            int code = Integer.parseInt(status_code);
            return valueFor(code);
        }
    }
    public static LoginServletError valueFor(int status_code){
        switch (status_code){
            case -1:
                return NONE;
            case 0:
                return NO_MATCH;
            case 1:
                return ACCESS_DENIED;
        }

        return NONE;
    }
}