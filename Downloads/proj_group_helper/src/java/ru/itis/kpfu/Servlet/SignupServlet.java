package ru.itis.kpfu.Servlet;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import ru.itis.kpfu.Model.Group;
import ru.itis.kpfu.Model.UserBlueprint;
import ru.itis.kpfu.Service.GroupService;
import ru.itis.kpfu.Service.Templater;
import ru.itis.kpfu.Service.UserService;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "SignupServlet", urlPatterns = {"/signup/*"})
public class SignupServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        HttpSession session = req.getSession();
        resp.setCharacterEncoding("UTF-8");

        if(LoginServlet.checkSessionLogged(session)){
            resp.sendRedirect("/profile");
            return;
        }

        SignupServletError error = SignupServletError.valueFor(req.getParameter("error"));
        this.displayContent(req, resp, error);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("UTF-8");
        SignupServletError error = this.trySignup(req);

        if(error == SignupServletError.NONE){
            String login = req.getParameter("login");
            String password = req.getParameter("password");
            if(LoginServlet.tryLogin(login, password, req.getSession())){
                resp.sendRedirect("/profile");
            }else{
                System.out.println("••• Failed to login after signup :c");
                resp.sendRedirect("/");
            }
        }else{
            System.out.println("••• Signup error: " + error.toString());
            this.displayContent(req, resp, error);
        }
    }

    //MARK: - Handling
    private SignupServletError trySignup(HttpServletRequest req){
        String login = req.getParameter("login");
        String password = req.getParameter("password");
        String password_double = req.getParameter("password_double");
        String fullname = req.getParameter("fullname");
        String email = req.getParameter("email");
        int id_group = Integer.parseInt(req.getParameter("id_group"));

        HttpSession session = req.getSession();
        UserService service = (UserService) session.getAttribute("user_service");
        if(service == null){
            service = new UserService();
            session.setAttribute("user_service", service);
        }

        if(login == null || password == null || password_double == null || fullname == null || email == null){
            return SignupServletError.EMPTY_FIELD;
        }
        else if(login.length() < 3 || login.length() > 16){
            return SignupServletError.WRONG_LOGIN;
        }
        else if(!service.checkLoginAvailable(login)){
            return SignupServletError.TAKEN_LOGIN;
        }
        else if(!password_double.equals(password) || password.length() < 6 || password.length() > 16){
            return SignupServletError.WRONG_PASSWORD;
        }
        else if(fullname.length() < 3 || fullname.length() > 16){
            return SignupServletError.WRONG_USERNAME;
        }
        else if(!service.checkEmailAvailable(email)){
            return SignupServletError.TAKEN_EMAIL;
        }

        UserBlueprint blueprint = new UserBlueprint();
        blueprint.setLogin(login);
        blueprint.setPassword(password);
        blueprint.setFullname(fullname);
        blueprint.setEmail(email);
        blueprint.setId_group(id_group);

        if(service.add(blueprint) == -1){
            return SignupServletError.INTERNAL;
        }

        return SignupServletError.NONE;
    }

    //MARK: - Convenience
    private void displayContent(HttpServletRequest req, HttpServletResponse resp, SignupServletError error) throws IOException {
        ServletContext context = req.getServletContext();
        Template templ = Templater.getTemplate(context, "signup.ftl");
        PrintWriter writer = resp.getWriter();

        Map<String, Object> data = new HashMap<>();
        data.putAll(error.getData());

        GroupService service = new GroupService();
        ArrayList<Group> groups = service.getAll();
        data.put("groups", groups);

        try {
            templ.process(data, writer);
        } catch (TemplateException e) {
            System.out.println("No templates were found(processed)");
            e.printStackTrace();
        }

        writer.flush();
        writer.close();
    }

}

enum SignupServletError{

    NONE (0, ""),
    INTERNAL (1, "Произошла внутренняя ошибка :с\nПожалуйста, повторите попытку!"),

    EMPTY_FIELD (100, "Пожалуйста, заполните ВСЕ обязательные поля"),

    TAKEN_LOGIN (200, "Такой логин уже занят"),
    TAKEN_EMAIL (201, "Такой email уже зарегистрирован"),

    WRONG_LOGIN (300, "Пожалуйста, укажите другой логин\n(длина: 3 < Логин < 16)"),
    WRONG_PASSWORD (301, "Пожалуйста, укажите другой пароль\n(длина: 6 < Пароль < 16)"),
    WRONG_USERNAME (302, "Пожалуйста, укажите другой псевдоним\n(длина: 3 < Псевдоним < 16)");

    private int code;
    private String message;

    SignupServletError(int code, String message) {
        this.code = code;
        this.message = message;
    }

    //MARK: - Convenience
    public HashMap<String, Object> getData(){
        HashMap<String, Object> map = new HashMap<>();

        switch (this){
            case NONE:
                break;

            default:
                map.put("msg_error", this.message);
                break;
        }

        return map;
    }
    public static  SignupServletError valueFor(String status_code){
        if(status_code == null){
            return NONE;
        }else{
            int code = Integer.parseInt(status_code);
            return valueFor(code);
        }
    }
    public static SignupServletError valueFor(int status_code){
        switch (status_code){
            case 1:
                return INTERNAL;

            case 100:
                return EMPTY_FIELD;

            case 200:
                return TAKEN_LOGIN;
            case 201:
                return TAKEN_EMAIL;

            case 300:
                return WRONG_LOGIN;
            case 301:
                return WRONG_PASSWORD;
            case 302:
                return WRONG_USERNAME;

            default:
                return NONE;
        }
    }

    @Override
    public String toString() {
        return this.message + " (" + String.valueOf(code) + ")";
    }
}