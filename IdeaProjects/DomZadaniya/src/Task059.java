public class Task059 {
    public static void main(String[] args) {
    }
}

class RationalComplexMatrix2x2 extends RationalComplexNumber{
    RationalComplexNumber[][] arr = new RationalComplexNumber[2][2];

    RationalComplexMatrix2x2(){
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                arr[i][j] = new RationalComplexNumber();
            }
        }
    }
    RationalComplexMatrix2x2(RationalComplexNumber num){
        for (int i = 0; i < 2; i++){
            for (int j = 0; j < 2; j++){
                arr[i][j] = num;
            }
        }
    }
    RationalComplexMatrix2x2(RationalComplexNumber m1, RationalComplexNumber m2, RationalComplexNumber m3, RationalComplexNumber m4){
        arr[0][0] = m1;
        arr[0][1] = m2;
        arr[1][0] = m3;
        arr[1][1] = m4;
    }
    public RationalComplexNumber getArr(int i, int j) {
        return arr[i][j];
    }

    RationalComplexMatrix2x2 add(RationalComplexMatrix2x2 Msv){
        RationalComplexNumber[][] arr1 = new RationalComplexNumber[2][2];
        for (int k = 0; k < 2; k++) {
            for (int i = 0; i < 2; i++) {
                arr1[k][i] = (arr[k][i]).add(Msv.getArr(k, i));
            }
        }
        return new RationalComplexMatrix2x2(arr1[0][0], arr1[0][1], arr1[1][0], arr1[1][1]);
    }

    RationalComplexMatrix2x2 mult(RationalComplexMatrix2x2 Msv){
        RationalComplexNumber[][] arr1 = new RationalComplexNumber[2][2];
        for (int k = 0; k < 2; k++) {
            for (int i = 0; i < 2; i++) {
                arr1[k][i] = (arr[k][i]).mult(Msv.getArr(k, i));
            }
        }
        return new RationalComplexMatrix2x2(arr1[0][0], arr1[0][1], arr1[1][0], arr1[1][1]);
    }

    RationalComplexNumber det(){
        RationalComplexNumber determinant = (arr[0][0].add(arr[1][1])).sub(arr[0][1].add(arr[1][0]));
        return determinant;
    }
}
