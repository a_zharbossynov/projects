package ru.itis.kpfu.Servlet;

import freemarker.template.Template;
import freemarker.template.TemplateException;
import ru.itis.kpfu.Model.Group;
import ru.itis.kpfu.Model.Lesson;
import ru.itis.kpfu.Model.Schedule;
import ru.itis.kpfu.Model.User;
import ru.itis.kpfu.Service.GroupService;
import ru.itis.kpfu.Service.ScheduleService;
import ru.itis.kpfu.Service.Templater;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@WebServlet(name = "ScheduleServlet", urlPatterns = {"/schedule/*"})
public class ScheduleServlet extends HttpServlet{

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("UTF-8");

        ProfileServlet.ensureVariables(req, (exist, user, service) -> {
            if(exist){
                this.displayContent(req, resp, user);
            }else{
                resp.sendRedirect(LoginServlet.LINK_ERROR_ACCESS);
            }
        });
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setCharacterEncoding("UTF-8");

        ProfileServlet.ensureVariables(req, (exist, user, service) -> {
            if(exist){
                if(req.getParameter("auditorium") != null){
                    this.handleTimetableUpdate(req, user);
                }
                else if(req.getParameter("hometask") != null){
                    this.handleHometaskUpdate(req, user);
                }
                else{
                    System.out.println("••• Post request not recognized");
                }

                this.displayContent(req, resp, user);
            }else{
                resp.sendRedirect(LoginServlet.LINK_ERROR_ACCESS);
            }
        });
    }

    //MARK: - Responding
    private void displayContent(HttpServletRequest req, HttpServletResponse resp, User user) throws IOException {
        ServletContext context = req.getServletContext();
        Template templ = Templater.getTemplate(context, "schedule.ftl");

        resp.setCharacterEncoding("UTF-8");
        PrintWriter writer = resp.getWriter();
        Map<String, Object> data = new HashMap<>();
        data.putAll(user.getHashMap());

        GroupService group_service = new GroupService();
        ArrayList<Group> groups = group_service.getAll();
        String notice = group_service.getNotice(user.getId_group());
        boolean editor = group_service.getEditorId(user.getId_group()) == user.getId();

        ScheduleService schedule_service = new ScheduleService();
        Schedule schedule = schedule_service.getSchedule(user.getId_group(), user.getId());

        data.put("groups", groups);
        data.put("notice", notice);
        data.put("is_editor", editor);
        data.put("schedule", schedule);

        try {
            templ.process(data, writer);
        } catch (TemplateException e) {
            System.out.println("No templates were found(processed)");
            e.printStackTrace();
        }

        writer.flush();
        writer.close();
    }

    //MARK: - Handling requests
    private void handleTimetableUpdate(HttpServletRequest request, User user){
        String s_weekday = request.getParameter("weekday");
        String s_ordinal = request.getParameter("ordinal");
        String auditorium = request.getParameter("auditorium");
        String title = request.getParameter("title");

        if(s_weekday != null && s_ordinal != null && auditorium != null && title != null){
            int ordinal = Integer.parseInt(s_ordinal);
            int weekday = Integer.parseInt(s_weekday);

            Lesson lesson = new Lesson(weekday, ordinal, title, auditorium, null);
            ScheduleService service = new ScheduleService();
            service.addLesson(lesson, user.getId_group(), weekday);
        }
    }
    private void handleHometaskUpdate(HttpServletRequest request, User user){
        String s_weekday = request.getParameter("weekday");
        String s_ordinal = request.getParameter("ordinal");
        String hometask = request.getParameter("hometask");

        if(s_weekday != null && s_ordinal != null && hometask != null){
            int ordinal = Integer.parseInt(s_ordinal);
            int weekday = Integer.parseInt(s_weekday);

            ScheduleService service = new ScheduleService();
            service.setHometask(hometask, user.getId_group(), weekday, ordinal);
        }
    }
}
