package hw_02_03_2017.Pattern_Composite;

/**
 * Created by MacBookAir on 06.03.17.
 */
public class Circle implements IShape {

    @Override
    public void draw(String fillColor) {
        System.out.println("Drawing Circle with color " + fillColor);
    }

}
