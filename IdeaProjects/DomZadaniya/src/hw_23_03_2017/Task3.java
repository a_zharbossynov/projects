package hw_23_03_2017;

import java.util.List;
import java.util.Arrays;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * Created by MacBookAir on 27.03.17.
 */

public class Task3 {
    public static void main(String[] args) {
        List<String> list = Arrays.asList("Dexter", "Yuki", "Kevin", "Miranda", "Kim", "Benjamin");
        List<String> listTwo = list.stream().
                map(n -> {
        if(n.length() < 4){
                return n.toUpperCase();
            }
            return n;
        }).filter(n -> n.length() < 4 || n.length() >= 4 && (n.contains("E")) || (n.contains("B")))
                .collect(Collectors.toList());
        listTwo.stream().forEach(n -> System.out.println(n + " "));
    }
}
