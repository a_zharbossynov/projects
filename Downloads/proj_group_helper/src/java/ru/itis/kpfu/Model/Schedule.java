package ru.itis.kpfu.Model;

public class Schedule {

    private DayTimetable[] schedule;

    //MARK: - Initializers
    public Schedule(){
        this.schedule = new DayTimetable[6];
        for (int i = 1; i <= 6 ; i++) {
            schedule[i - 1] = new DayTimetable(i);
        }
    }
    public void addLesson(Lesson lesson){
        int weekday = lesson.getWeekday();
        if(weekday <= this.schedule.length){
            this.schedule[weekday - 1].addLesson(lesson);
        }
    }

    //MARK: - Getters
    public DayTimetable[] getSchedule() {
        if(this.schedule == null){
            return new DayTimetable[6];
        }

        return schedule;
    }
}
