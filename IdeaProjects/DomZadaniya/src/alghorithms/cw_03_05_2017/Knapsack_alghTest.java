package alghorithms.cw_03_05_2017;

import org.junit.Test;
import static org.junit.Assert.*;

/**
 * Created by MacBookAir on 03.05.17.
 */
public class Knapsack_alghTest {
    Knapsack_algh a = new Knapsack_algh();

    @Test
    public void max() {
        int x = 6;
        int y = 10;
        int exp = y;
        int result = a.max(x,y);
        assertEquals(exp, result);

    }



    @Test
    public void knapsackAlghorithmTest(){
        int cost[] = new int[]{12, 10, 20, 15};
        int weight[] = new int[]{2, 1, 3, 2};
        int W = 5;
        int n = cost.length;
        int exp = 37;
        assertEquals(exp, a.knapsack(W, weight, cost, n));
    }

}