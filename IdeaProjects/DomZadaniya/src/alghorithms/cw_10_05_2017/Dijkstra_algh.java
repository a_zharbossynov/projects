package alghorithms.cw_10_05_2017;

import java.util.LinkedList;
import java.util.PriorityQueue;

/**
 * Created by MacBookAir on 10.05.17.
 */
public class Dijkstra_algh {

        public static void main(String[] arg){

            Dijkstra_algh t = new Dijkstra_algh();

            // Создаем граф
            Graph g = new Graph(5);

            // Добавляем дуги(источник, конец, вес)
            g.addEdge(0, 1, 3);
            g.addEdge(0, 3, 7);
            g.addEdge(1, 0, 3);
            g.addEdge(1, 2, 1);
            g.addEdge(1, 3, 2);
            g.addEdge(2, 1, 1);
            g.addEdge(2, 3, 4);
            g.addEdge(2, 4, 6);
            g.addEdge(3, 0, 7);
            g.addEdge(3, 1, 2);
            g.addEdge(3, 2, 4);
            g.addEdge(3, 4, 4);
            g.addEdge(4, 2, 6);
            g.addEdge(4, 3, 4);

            t.calcPath(g.getVertex(0));

            for(Vertex v: g.getVertices()){
                System.out.print("Вершина - " + v + " , Расстояние - " + v.minDistance + " , Путь - ");
                for(Vertex pathvert: v.path) {
                    System.out.print(pathvert + " ");
                }
                System.out.println("" + v);
            }

        }

    // 1. Берем непройденный узел с минимальным весом.
    // 2. Проходим по всем его соседним узлам.
    // 3. И обновляем расстояние для всех его соседей (используя Priority Queue).
    // Повторяем до тех пор, пока все подключенные узлы не будут пройдены.

        public void calcPath(Vertex src){

            src.minDistance = 0;
            PriorityQueue<Vertex> queue = new PriorityQueue<Vertex>();
            queue.add(src);

            while(!queue.isEmpty()){

                Vertex u = queue.poll();

                for(Edge neighbor: u.neighbors){
                    Double newDist = u.minDistance + neighbor.weight;

                    if(neighbor.target.minDistance > newDist){
                        queue.remove(neighbor.target);
                        neighbor.target.minDistance = newDist;
                        neighbor.target.path = new LinkedList<Vertex>(u.path);
                        neighbor.target.path.add(u);
                        queue.add(neighbor.target);
                    }
                }
            }
        }




}
