package ru.itis.kpfu.Model;

import java.sql.ResultSet;
import java.sql.SQLException;

public class Lesson {

    private int weekday;
    private int ordinal;
    private String name;
    private String auditorium;
    private String hometask;

    private boolean is_empty = false;

    //MARK: - Initializers
    public Lesson(ResultSet set) throws SQLException {
        this.weekday = set.getInt("weekday");
        this.ordinal = set.getInt("ordinal");
        this.name = set.getString("title");
        this.auditorium = set.getString("auditorium");

        if(set.getMetaData().getColumnCount() > 4) {
            this.hometask = set.getString("hometask");
        }
    }
    public Lesson(int weekday, int ordinal, String name, String auditorium, String hometask) {
        this.weekday = weekday;
        this.ordinal = ordinal;
        this.name = name;
        this.auditorium = auditorium;
        this.hometask = hometask;
    }

    //MARK: - Filling
    private Lesson(int ordinal){
        this.ordinal = ordinal;
        this.is_empty = true;
    }

    //MARK: - Getters
    public String getTimeframe(){
        switch (ordinal){
            case 0:
                return "8:30 – 10:00";
            case 1:
                return "10:10 – 11:40";
            case 2:
                return "11:50 – 13:20";
            case 3:
                return "13:40 - 15:10";
            case 4:
                return "15:20 - 16:50";
            case 5:
                return "17:00 - 18:30";

            default:
                return "overtime";
        }
    }
    public int getWeekday() {
        return weekday;
    }
    public int getOrdinal() {
        return ordinal;
    }
    public String getName() {
        if(this.is_empty || this.name == null){
            return "–";
        }

        return name;
    }
    public String getAuditorium() {
        if(this.is_empty || this.auditorium == null){
            return "–";
        }

        return auditorium;
    }
    public String getHometask() {
        if(this.is_empty || this.hometask == null){
            return "–";
        }

        return hometask;
    }
    public boolean getIsEmpty(){
        return this.is_empty;
    }

    //MARK: - Merging
    public Lesson merged(Lesson overlay){
        int weekday = this.getWeekday();
        int ordinal = this.getOrdinal();
        String name = this.getName() + ",  " + overlay.getName();
        String auditorium = "(" + this.getName() + ") " + this.getAuditorium()
                + ",  (" + overlay.getName() + ") " + overlay.getAuditorium();
        String hometask = "(" + this.getName() + ") " + this.getHometask()
                + ",  (" + overlay.getName() + ") " + overlay.getHometask();

        return new Lesson(weekday, ordinal, name, auditorium, hometask);
    }

    //MARK: - Static fabrics
    public static Lesson getEmptyLesson(int ordinal){
        return new Lesson(ordinal);
    }
}
