import java.util.Scanner;
import java.util.Random;

public class Task048 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите имя преподователя: ");
        System.out.println("Введите название предмета: ");
        Prepodovatel p1 = new Prepodovatel(sc.nextLine(), sc.nextLine());
        System.out.println("Введите имя студента: ");
        Student s1 = new Student(sc.nextLine());
        System.out.print("Преподователь " + p1.getFullName() + " оценил студента с именем " + s1.getStudentName());
        System.out.println(" по предмету " + p1.getSubject() + " на оценку " + p1.ocenka() + " !");
    }
}

class Prepodovatel {
    private String fullName;
    private String subject;
    private int score;

    public Prepodovatel(String fullName, String subject) {
        this.fullName = fullName;
        this.subject = subject;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getFullName() {
        return fullName;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    int ocenka(){
        Random ocenka = new Random();
        int n = ocenka.nextInt(4) + 2;
        return n;
    }
}

class Student{
    private String studentName;

    public Student(String studentName){
        this.studentName = studentName;
    }

    public void setStudentName(String studentName){
        this.studentName = studentName;
    }

    public String getStudentName(){
        return studentName;
    }


}
