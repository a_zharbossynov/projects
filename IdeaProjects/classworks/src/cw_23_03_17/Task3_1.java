package cw_23_03_17;

/**
 * Created by MacBookAir on 23.03.17.
 *
 * Проверочная
 * Задание номер 3.
 */
public class Task3_1 {
    public static void main(String[] args) {
        String str1 = "Porsche";
        String str2 = "Maserati";

        WhichOneBest bestString = (obj1, obj2) -> (obj1.compareTo(obj2) < 0) ? obj1 : obj2;
        System.out.println(bestString.func(str1, str2));
    }
}

@FunctionalInterface
interface WhichOneBestGen<T>{
    T func(T str1, T str2);
}

