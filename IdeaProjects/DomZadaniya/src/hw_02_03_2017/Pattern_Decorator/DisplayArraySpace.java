package hw_02_03_2017.Pattern_Decorator;


public class DisplayArraySpace extends ArrayDisplayDecorator {

    private String [] arr;
    public DisplayArraySpace(IArrayString basicDisplay){
        originalDisplay = basicDisplay;
    }
    public String[] getArr(){
        return arr;
    }

    @Override
    public String toDisplay(){
        return
                toDisplayWithSpace(originalDisplay);
    }
    public String toDisplayWithSpace(IArrayString display){

        return display.toDisplay().replaceAll(","," ");
    }
}
