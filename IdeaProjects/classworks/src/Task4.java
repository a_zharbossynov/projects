/**
 * Created on 01.12.16.
 * Task-4
 */
import java.util.Scanner;
public class Task4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите: f, s, n. ");
        int f = sc.nextInt();
        int s = sc.nextInt();
        int n = sc.nextInt();
        System.out.println(getNumber(f,s,n));
    }

    public static int getNumber(int f, int s, int n){
        int num = ((n - f)/s) + 1;
        if (num > 0){
            return num;
        }else {
            num = -1;
            return num;
        }
    }

}
