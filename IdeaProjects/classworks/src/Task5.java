/**
 * Created on 01.12.16.
 * Task-5
 */
import java.util.Scanner;
public class Task5 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Введите вещественное число n и степень k: ");
        double n = sc.nextDouble();
        int k = sc.nextInt();
        System.out.println(getDegree(n, k));
    }

    public static double getDegree(double a, int n) {
        if (n == 0)
            return 1;
        if (n % 2 == 1)
            return getDegree (a, n-1) * a;
        else {
            double b = getDegree (a, n/2);
            return b * b;
        }
    }


}
