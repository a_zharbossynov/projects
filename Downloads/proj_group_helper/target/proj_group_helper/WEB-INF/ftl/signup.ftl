<#-- @ftlvariable name="groups" type="com.sun.tools.javac.util.List<ru.itis.kpfu.Model.Group>" -->
<#-- @ftlvariable name="msg_error" type="java.lang.String" -->
<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="http://getbootstrap.com/favicon.ico">

    <title>Регистрация</title>

    <!-- Bootstrap core CSS -->
    <link href="/resources/css/bootstrap.css" rel="stylesheet">
    <link href="http://getbootstrap.com/dist/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="http://getbootstrap.com/docs/4.0/examples/offcanvas/offcanvas.css" rel="stylesheet">
</head>

<body>

<nav class="navbar navbar-expand-md fixed-top navbar-dark" style="background-color: #0c2954;">
    <a class="navbar-brand" href="#">Group helper</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
        </ul>
        <ul class="nav justify-content-end">
            <li class="nav-item">
                <a href="/login/" class="btn btn-outline-light" role="button">Войти</a>
            </li>
        </ul>
    </div>
</nav>

<div class="container">
    <div class="row align-items-center">
        <div class="col-12">
            <div class="alert alert-light" role="alert">
                <h2 class="text-center">Казанский (Приволжский) федеральный университет – Высшая школа информационных
                    технологий и информационных систем</h2>
            </div>
            <div class="row align-items-center">
                <div class="col-6 mx-auto">
                    <h1 class="text-center">Регистрация</h1><br>
                    <h4 class="text-center" id="lbl_status" style="color: <#if msg_error??>orangered<#else>slategrey</#if>">
                    <#if msg_error??>ОШИБКА: ${msg_error}<#else>Заполните ВСЕ поля</#if>
                    </h4><br>
                    <form action="/signup/" method="post">
                        <div class="form-group">
                            <input type="text" name="login" class="form-control" placeholder="Введите логин (вводится при входе)" required>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password" class="form-control" placeholder="Введите пароль" required>
                        </div>
                        <div class="form-group">
                            <input type="password" name="password_double" class="form-control" placeholder="Введите пароль ещё раз" required>
                        </div>
                        <div class="form-group">
                            <input type="text" name="fullname" class="form-control" placeholder="Псевдоним (как вас будут видеть остальные)" required>
                        </div>
                        <div class="form-group">
                            <input type="email" name="email" class="form-control" placeholder="E-mail" required>
                        </div>
                        <#if groups??>
                            <div class="form-group">
                                <label for="electives">Учебная группа</label>
                                <select class="form-control" id="electives" name="id_group">
                                    <#list groups as group>
                                        <option value="${group.getId()}">${group.getTitle()}</option>
                                    </#list>
                                </select>
                                <#--<input type="text" name="id_group" class="form-control" placeholder="Учебная группа" required>-->
                            </div>
                        </#if>
                        <button type="submit" class="btn btn-primary btn-lg btn-block">Зарегистрироваться</button>
                    </form>
                </div>
            </div>
        </div><!--/span-->
    </div><!--/row-->

</div><!--/.container-->


<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="http://getbootstrap.com/assets/js/vendor/popper.min.js"></script>
<script src="http://getbootstrap.com/dist/js/bootstrap.min.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="http://getbootstrap.com/assets/js/ie10-viewport-bug-workaround.js"></script>
<script src="http://getbootstrap.com/docs/4.0/examples/offcanvas/offcanvas.js"></script>
</body>
</html>
