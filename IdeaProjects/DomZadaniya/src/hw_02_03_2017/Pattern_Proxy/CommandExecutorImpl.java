package hw_02_03_2017.Pattern_Proxy;

/**
 * Created by MacBookAir on 06.03.17.
 */
import java.io.IOException;

public class CommandExecutorImpl implements ICommandExecutor {

    @Override
    public void runCommand(String cmd) throws IOException {
        //some heavy implementation
        Runtime.getRuntime().exec(cmd);
        System.out.println("'" + cmd + "' command executed.");
    }

}



