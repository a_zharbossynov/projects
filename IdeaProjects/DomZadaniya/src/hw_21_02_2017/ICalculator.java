package hw_21_02_2017;

/**
 * Created by MacBookAir on 27.02.17.
 */
public interface ICalculator {
        public double add(double a, double b);
        public double sub(double a, double b);
        public double mult(double a, double b);
        public double div(double a, double b);
}

