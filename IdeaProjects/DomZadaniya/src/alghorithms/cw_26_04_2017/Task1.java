package alghorithms.cw_26_04_2017;

/**
 * Created by MacBookAir on 26.04.17.
 *
 */

import java.io.PrintWriter;
import java.util.Scanner;

public class Task1 {
    private static final int INFINITY = 1000000000;
    public static void main(String[] args) {
        Task1 t = new Task1();

        t.alghorithm();
    }

    private void alghorithm() {
        Scanner scanner = new Scanner(System.in);
        PrintWriter printWriter = new PrintWriter(System.out);

        System.out.println("Введите кол-во вершин: ");
        int vCount = scanner.nextInt();
        int[][] matrix = new int[vCount][vCount];
        System.out.println("Заполните матрицу: ");
        System.out.println("Примечание:  0 заменяет бесконечность");
        for (int i = 0; i < vCount; i++) {
            for (int j = 0; j < vCount; j++) {
                matrix[i][j] = scanner.nextInt();
                if (matrix[i][j] == 0) {
                    matrix[i][j] = INFINITY;
                }
            }
        }

        for (int k = 0; k < vCount; k++) {
            for (int i = 0; i < vCount; i++) {
                for (int j = 0; j < vCount; j++) {
                    matrix[i][j] = Math.min(matrix[i][j], matrix[i][k] + matrix[k][j]);
                }
            }
        }

        for (int i = 0; i < vCount; i++) {
            for (int j = 0; j < vCount; j++) {
                if (matrix[i][j] == INFINITY) {
                    printWriter.print(0 + " ");
                } else {
                    printWriter.print(matrix[i][j] + " ");
                }
            }

            printWriter.println();
        }

        scanner.close();
        printWriter.close();
    }
}
