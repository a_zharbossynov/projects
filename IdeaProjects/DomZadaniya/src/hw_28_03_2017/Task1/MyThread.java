package hw_28_03_2017.Task1;

/**
 * Created by MacBookAir on 30.03.17.
 */
public class MyThread implements Runnable{

    @Override
    public void run() {
        int result = getMax();
        System.out.println(result);
    }

    public static int getMax(){
        int[] arr = new int[]{3,4,5,6,2,4,19,0};
        int maxIndex = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[maxIndex] < arr[i]) {
                maxIndex = i;
            }
        }
        return arr[maxIndex];
    }



}
