public class Task056 {
    public static void main(String[] args) {
    }
}

class ComplexMatrix2x2 extends ComplexNumber{
    ComplexNumber[][] arr = new ComplexNumber[2][2];

    ComplexMatrix2x2(){
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                arr[i][j] = new ComplexNumber();
            }
        }
    }
    ComplexMatrix2x2(ComplexNumber num){
        for (int i = 0; i < 2; i++){
            for (int j = 0; j < 2; j++){
                arr[i][j] = num;
            }
        }
    }
    ComplexMatrix2x2(ComplexNumber m1, ComplexNumber m2, ComplexNumber m3, ComplexNumber m4){
        arr[0][0] = m1;
        arr[0][1] = m2;
        arr[1][0] = m3;
        arr[1][1] = m4;
    }

    public ComplexNumber getArr(int i, int j) {
        return arr[i][j];
    }

    ComplexMatrix2x2 add(ComplexMatrix2x2 Msv){
        ComplexNumber[][] arr1 = new ComplexNumber[2][2];
        for (int k = 0; k < 2; k++) {
            for (int i = 0; i < 2; i++) {
                arr1[k][i] = (arr[k][i]).add(Msv.getArr(k, i));
            }
        }
        return new ComplexMatrix2x2(arr1[0][0], arr1[0][1], arr1[1][0], arr1[1][1]);
    }

    ComplexMatrix2x2 mult(ComplexMatrix2x2 Msv){
        ComplexNumber[][] arr1 = new ComplexNumber[2][2];
        for (int k = 0; k < 2; k++) {
            for (int i = 0; i < 2; i++) {
                arr1[k][i] = (arr[k][i]).mult(Msv.getArr(k, i));
            }
        }
        return new ComplexMatrix2x2(arr1[0][0], arr1[0][1], arr1[1][0], arr1[1][1]);
    }
    ComplexNumber det(){
        ComplexNumber determinant = (arr[0][0].add(arr[1][1])).sub(arr[0][1].add(arr[1][0]));
        return determinant;
    }

}
