package hw_02_03_2017.Pattern_Proxy;

/**
 * Created by MacBookAir on 06.03.17.
 */
public interface ICommandExecutor {
    public void runCommand(String cmd) throws Exception;
}
