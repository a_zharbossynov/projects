package hw_21_02_2017;

/**
 * Created by MacBookAir on 27.02.17.
 */
public interface IExtraFunctions {
    public double square(double a);
    public double sqRoot(double a);
    public double cosinus(double a);
    public double sinus(double a);
}
