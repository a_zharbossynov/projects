public class Task054 {
    public static void main(String[] args) {
    }
}

class ComplexVector2D extends ComplexNumber{
    ComplexNumber x, y;
    ComplexVector2D(){
        x.a = 0;
        x.b = 0;
        y.a = 0;
        y.b = 0;
    }

    public ComplexNumber getX() {
        return x;
    }

    public ComplexNumber getY() {
        return y;
    }
    ComplexVector2D(ComplexNumber x, ComplexNumber y){
        this.x = x;
        this.y = y;
    }
    ComplexVector2D add(ComplexVector2D vector2D){
        ComplexNumber sumX = x.add(vector2D.getX());
        ComplexNumber sumY = y.add(vector2D.getY());
        return new ComplexVector2D(sumX, sumY);
    }
    public String toString() {
        String s = "(" + x.toString() + ";" + y.toString() + ")";
    return s;
    }
    ComplexNumber scalarProduct(ComplexVector2D vector2D){
        ComplexNumber scProduct = (x.mult(vector2D.getX())).add(y.mult(vector2D.getY()));
        return  scProduct;
    }
    boolean equals(ComplexVector2D vector2D){
        boolean vector = x.equals(vector2D.getX()) && y.equals(vector2D.getY());
        return vector;
    }
}
