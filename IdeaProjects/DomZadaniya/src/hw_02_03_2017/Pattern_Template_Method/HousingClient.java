package hw_02_03_2017.Pattern_Template_Method;

/**
 * Created by MacBookAir on 06.03.17.
 */
public class HousingClient {
    public static void main(String[] args) {
        HouseTemplate houseType = new WoodenHouse();

        //using template method
        houseType.buildHouse();
        System.out.println("************");

        houseType = new GlassHouse();

        houseType.buildHouse();
    }
}
