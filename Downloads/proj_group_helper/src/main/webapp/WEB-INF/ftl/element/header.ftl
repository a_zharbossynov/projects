<nav class="navbar navbar-expand-md fixed-top navbar-dark" style="background-color: #0c2954;">
    <a class="navbar-brand" href="#">Group helper</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault"
            aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
        <ul class="navbar-nav mr-auto">
        </ul>
        <ul class="nav justify-content-end">
            <li class="nav-item">
                <a href="/signup/" class="btn btn-outline-light" role="button">Зарегистрироваться</a>
            </li>
        </ul>
    </div>
</nav>