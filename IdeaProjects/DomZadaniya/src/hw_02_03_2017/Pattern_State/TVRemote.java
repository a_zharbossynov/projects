package hw_02_03_2017.Pattern_State;

/**
 * Created by MacBookAir on 06.03.17.
 */

public class TVRemote {

    public static void main(String[] args) {
        TVContext context = new TVContext();
        IState tvStartState = new TVStartState();
        IState tvStopState = new TVStopState();

        context.setState(tvStartState);
        context.doAction();

        context.setState(tvStopState);
        context.doAction();

    }

}
